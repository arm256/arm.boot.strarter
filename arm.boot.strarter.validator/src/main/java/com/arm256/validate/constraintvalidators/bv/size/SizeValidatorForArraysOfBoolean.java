
package com.arm256.validate.constraintvalidators.bv.size;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.SizeConstraint;

@Service("SizeArrayBoolean")
public class SizeValidatorForArraysOfBoolean extends SizeValidatorForArray<Boolean> {

	public SizeValidatorForArraysOfBoolean(SizeConstraint p) {
		super(p);
	}

}
