
package com.arm256.validate.constraintvalidators.bv.size;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.SizeConstraint;

@Service("SizeArrayChar")
public class SizeValidatorForArraysOfChar extends SizeValidatorForArray<Character> {

	public SizeValidatorForArraysOfChar(SizeConstraint p) {
		super(p);
	}

}