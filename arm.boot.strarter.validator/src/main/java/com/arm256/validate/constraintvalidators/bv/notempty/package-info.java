

/**
 * Implementations of the Bean Validation {@code NotEmpty} constraint.
 */
package com.arm256.validate.constraintvalidators.bv.notempty;
