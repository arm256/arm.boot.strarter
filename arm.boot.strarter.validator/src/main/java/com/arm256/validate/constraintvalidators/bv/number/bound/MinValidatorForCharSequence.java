
package com.arm256.validate.constraintvalidators.bv.number.bound;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.MinConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@Service("MinString")
public class MinValidatorForCharSequence extends AbstractConstraintValidator<CharSequence> {

	public MinValidatorForCharSequence(MinConstraint p) {
		super(p);
	}

	@Override
	public boolean isValid(CharSequence value, int context) {
		// null values are valid
		if (value == null) {
			return false;
		}

		long min = getParams(context, 0);

		try {
			return NumberComparatorHelper.compare(new BigDecimal(value.toString()), min,
					NumberComparatorHelper.LESS_THAN) <= 0;
		}
		catch (NumberFormatException nfe) {
			return false;
		}
	}

}
