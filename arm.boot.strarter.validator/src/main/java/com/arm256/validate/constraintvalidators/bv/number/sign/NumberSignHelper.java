
package com.arm256.validate.constraintvalidators.bv.number.sign;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.OptionalInt;
import java.util.function.ToIntBiFunction;

import com.arm256.validate.constraintvalidators.bv.number.InfinityNumberComparatorHelper;

final class NumberSignHelper {

	private NumberSignHelper() {
	}

	private static final short SHORT_ZERO = (short) 0;

	private static final byte BYTE_ZERO = (byte) 0;

	public static final OptionalInt LESS_THAN = OptionalInt.of(-1);
	public static final OptionalInt FINITE_VALUE = OptionalInt.empty();
	public static final OptionalInt GREATER_THAN = OptionalInt.of(1);

	public static ToIntBiFunction<Long, OptionalInt> longFun = (e, v) -> Long.signum(e);
	public static ToIntBiFunction<Integer, OptionalInt> intFun = (e, v) -> Integer.signum(e);
	public static ToIntBiFunction<Short, OptionalInt> shortFun = (e, v) -> e.compareTo(SHORT_ZERO);
	public static ToIntBiFunction<Byte, OptionalInt> byteFun = (e, v) -> e.compareTo(BYTE_ZERO);
	public static ToIntBiFunction<BigInteger, OptionalInt> bigIntFun = (e, v) -> e.signum();
	public static ToIntBiFunction<BigDecimal, OptionalInt> bigDecimalFun = (e, v) -> e.signum();

	public static ToIntBiFunction<Float, OptionalInt> floatFunLess = (e, v) -> signum(e, v);
	public static ToIntBiFunction<Double, OptionalInt> doubleFunLess = (e, v) -> signum(e, v);

	public static ToIntBiFunction<Number, OptionalInt> numberFun = (e, v) -> Double.compare(e.doubleValue(), 0D);

	public static Class<?>[] clazzs = new Class[] { Long.class, Integer.class, Short.class, Byte.class,
			BigInteger.class, BigDecimal.class, Float.class, Double.class, Number.class };

	@SuppressWarnings("unchecked")
	public static ToIntBiFunction<Object, OptionalInt>[] funs = new ToIntBiFunction[] { longFun, intFun, shortFun,
			byteFun, bigIntFun,
			bigDecimalFun, floatFunLess, doubleFunLess, numberFun };

	public static <E extends Number> int signum(E e, OptionalInt oi) {
		int index = -1;

		loop: for (Class<?> clazz : clazzs) {
			index++;
			if (e.getClass().isAssignableFrom(clazz)) {
				break loop;
			}
		}

		return funs[index].applyAsInt(e, oi);
	}


	static int signum(Float number, OptionalInt treatNanAs) {
		OptionalInt infinity = InfinityNumberComparatorHelper.infinityCheck(number, treatNanAs);
		if (infinity.isPresent()) {
			return infinity.getAsInt();
		}
		return number.compareTo(0F);
	}

	static int signum(Double number, OptionalInt treatNanAs) {
		OptionalInt infinity = InfinityNumberComparatorHelper.infinityCheck(number, treatNanAs);
		if (infinity.isPresent()) {
			return infinity.getAsInt();
		}
		return number.compareTo(0D);
	}
}
