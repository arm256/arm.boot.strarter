
package com.arm256.validate.constraintvalidators.bv.money;

import javax.money.MonetaryAmount;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NegativeOrZero;

/**
 * Check that the number being validated positive.
 *
 * @author Marko Bekhta
 */
public class NegativeOrZeroValidatorForMonetaryAmount implements ConstraintValidator<NegativeOrZero, MonetaryAmount> {

	@Override
	public boolean isValid(MonetaryAmount value, ConstraintValidatorContext context) {
		// null values are valid
		if ( value == null ) {
			return true;
		}

		return value.signum() <= 0;
	}
}
