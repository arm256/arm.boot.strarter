package com.arm256.validate.constraintvalidators.bv;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.DigitsConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@Service("DigitsNumber")
public class DigitsValidatorForNumber extends AbstractConstraintValidator<Number> {

	public DigitsValidatorForNumber(DigitsConstraint c) {
		super(c);
	}
	@Override
	public boolean isValid(Number num, int context) {
		//null values are valid
		if ( num == null ) {
			return false;
		}

		BigDecimal bigNum;
		if ( num instanceof BigDecimal ) {
			bigNum = (BigDecimal) num;
		}
		else {
			bigNum = new BigDecimal( num.toString() ).stripTrailingZeros();
		}
		int maxIntegerLength = getParams(context, 0);
		int maxFractionLength = getParams(context, 1);

		int integerPartLength = bigNum.precision() - bigNum.scale();
		int fractionPartLength = bigNum.scale() < 0 ? 0 : bigNum.scale();

		return ( maxIntegerLength >= integerPartLength && maxFractionLength >= fractionPartLength );
	}


}
