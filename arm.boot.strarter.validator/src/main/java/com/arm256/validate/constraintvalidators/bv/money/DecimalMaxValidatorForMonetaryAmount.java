
package com.arm256.validate.constraintvalidators.bv.money;

import java.math.BigDecimal;

import javax.money.MonetaryAmount;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.DecimalMax;


/**
 * Check that the monetary amount being validated is less than or equal to the maximum
 * value specified.
 *
 * @author Lukas Niemeier
 * @author Willi Schönborn
 */
public class DecimalMaxValidatorForMonetaryAmount implements ConstraintValidator<DecimalMax, MonetaryAmount> {


	private BigDecimal maxValue;
	private boolean inclusive;

	@Override
	public void initialize(DecimalMax maxValue) {
		try {
			this.maxValue = new BigDecimal( maxValue.value() );
		}
		catch (NumberFormatException nfe) {
			throw new IllegalArgumentException(maxValue.value() + " does not represent a valid BigDecimal format.");
		}
		this.inclusive = maxValue.inclusive();
	}

	@Override
	public boolean isValid(MonetaryAmount value, ConstraintValidatorContext context) {
		// null values are valid
		if ( value == null ) {
			return true;
		}

		int comparisonResult = value.getNumber().numberValueExact( BigDecimal.class ).compareTo( maxValue );
		return inclusive ? comparisonResult <= 0 : comparisonResult < 0;
	}

}
