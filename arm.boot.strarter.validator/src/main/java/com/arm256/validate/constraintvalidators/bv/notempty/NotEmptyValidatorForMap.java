
package com.arm256.validate.constraintvalidators.bv.notempty;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.arm256.validator.AbstractConstraintValidator;


@SuppressWarnings("rawtypes")
@Service("NotEmptyMap")
public class NotEmptyValidatorForMap extends AbstractConstraintValidator<Map> {

	/**
	 * Checks the map is not {@code null} and not empty.
	 *
	 * @param map the map to validate
	 * @param constraintValidatorContext context in which the constraint is evaluated
	 * @return returns {@code true} if the map is not {@code null} and the map is not empty
	 */
	@Override
	public boolean isValid(Map map, int context) {
		if ( map == null ) {
			return false;
		}
		return map.size() > 0;
	}
}
