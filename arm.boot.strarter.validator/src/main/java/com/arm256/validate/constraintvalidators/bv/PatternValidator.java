package com.arm256.validate.constraintvalidators.bv;

import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.PatternConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@Service("Pattern")
public class PatternValidator extends AbstractConstraintValidator<CharSequence> {

	public PatternValidator(PatternConstraint pa) {
		super(pa);
	}

	@Override
	public boolean isValid(CharSequence value, int context) {
		if ( value == null ) {
			return true;
		}

		Pattern pattern = getParams(context, 0);

		return pattern.matcher(value).matches();
	}

}
