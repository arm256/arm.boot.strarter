
package com.arm256.validate.constraintvalidators.bv.notempty;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.arm256.validator.AbstractConstraintValidator;

@SuppressWarnings("rawtypes")
@Service("NotEmptyCollection")
public class NotEmptyValidatorForCollection extends AbstractConstraintValidator<Collection> {

	/**
	 * Checks the collection is not {@code null} and not empty.
	 *
	 * @param collection the collection to validate
	 * @param constraintValidatorContext context in which the constraint is evaluated
	 * @return returns {@code true} if the collection is not {@code null} and the collection is not empty
	 */
	@Override
	public boolean isValid(Collection collection, int context) {
		if ( collection == null ) {
			return false;
		}
		return collection.size() > 0;
	}
}
