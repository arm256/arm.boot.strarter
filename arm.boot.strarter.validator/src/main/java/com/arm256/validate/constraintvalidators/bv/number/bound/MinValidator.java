
package com.arm256.validate.constraintvalidators.bv.number.bound;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.MinConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@Service("Min")
public class MinValidator extends AbstractConstraintValidator<Number> {

	public MinValidator(MinConstraint p) {
		super(p);
	}

	@Override
	public boolean isValid(Number value, int context) {
		// null values are valid
		if (value == null) {
			return false;
		}

		long min = getParams(context, 0);

		return NumberComparatorHelper.compare(value, min, NumberComparatorHelper.LESS_THAN) >= 0;
	}

}
