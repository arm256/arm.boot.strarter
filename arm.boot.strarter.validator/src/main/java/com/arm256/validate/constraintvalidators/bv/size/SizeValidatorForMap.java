
package com.arm256.validate.constraintvalidators.bv.size;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.SizeConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@SuppressWarnings("rawtypes")
@Service("SizeMap")
public class SizeValidatorForMap extends AbstractConstraintValidator<Map> {

	public SizeValidatorForMap(SizeConstraint p) {
		super(p);
	}


	/**
	 * Checks the number of entries in a map.
	 *
	 * @param map The map to validate.
	 * @param constraintValidatorContext context in which the constraint is evaluated.
	 *
	 * @return Returns {@code true} if the map is {@code null} or the number of entries in {@code map}
	 *         is between the specified {@code min} and {@code max} values (inclusive),
	 *         {@code false} otherwise.
	 */
	@Override
	public boolean isValid(Map map, int context) {
		if ( map == null ) {
			return false;
		}

		int min = getParams(context, 0);
		int max = getParams(context, 1);

		int size = map.size();
		return size >= min && size <= max;
	}

}
