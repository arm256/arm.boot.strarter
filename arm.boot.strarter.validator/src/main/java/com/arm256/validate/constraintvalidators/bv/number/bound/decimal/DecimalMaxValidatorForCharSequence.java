
package com.arm256.validate.constraintvalidators.bv.number.bound.decimal;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.bv.number.InfinityNumberComparatorHelper;
import com.arm256.validate.constraintvalidators.params.DecimalMaxConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@Service("DecimalMaxString")
public class DecimalMaxValidatorForCharSequence extends AbstractConstraintValidator<CharSequence> {

	public DecimalMaxValidatorForCharSequence(DecimalMaxConstraint dm) {
		super(dm);
	}

	@Override
	public boolean isValid(CharSequence value, int context) {
		// null values are valid
		if (value == null) {
			return false;
		}
		BigDecimal maxValue = getParams(context, 0);
		boolean inclusive = getParams(context, 1);
		try {
			int comparisonResult = DecimalNumberComparatorHelper.compare(new BigDecimal(value.toString()), maxValue,
					InfinityNumberComparatorHelper.GREATER_THAN);
			return inclusive ? comparisonResult <= 0 : comparisonResult < 0;

		} catch (NumberFormatException nfe) {
			return false;
		}
	}

}
