
package com.arm256.validate.constraintvalidators.bv.size;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.SizeConstraint;

@Service("SizeArrayFloat")
public class SizeValidatorForArraysOfFloat extends SizeValidatorForArray<Float> {

	public SizeValidatorForArraysOfFloat(SizeConstraint p) {
		super(p);
	}

}