
package com.arm256.validate.constraintvalidators.bv.size;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.SizeConstraint;

@Service("SizeArrayLong")
public class SizeValidatorForArraysOfLong extends SizeValidatorForArray<Long> {

	public SizeValidatorForArraysOfLong(SizeConstraint p) {
		super(p);
	}

}