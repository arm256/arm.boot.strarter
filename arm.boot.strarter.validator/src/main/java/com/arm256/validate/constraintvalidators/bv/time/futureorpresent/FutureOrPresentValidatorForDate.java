
package com.arm256.validate.constraintvalidators.bv.time.futureorpresent;

import java.time.Instant;
import java.util.Date;

/**
 * Check that the {@code java.util.Date} passed to be validated is in the
 * future.
 *
 * @author Alaa Nassef
 * @author Guillaume Smet
 */
public class FutureOrPresentValidatorForDate extends AbstractFutureOrPresentInstantBasedValidator<Date> {

	@Override
	protected Instant getInstant(Date value) {
		return value.toInstant();
	}

}
