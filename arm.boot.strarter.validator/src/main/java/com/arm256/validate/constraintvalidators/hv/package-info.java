/*
 * Hibernate Validator, declare and validate application constraints
 *
 * License: Apache License, Version 2.0
 * See the license.txt file in the root directory or <http://www.apache.org/licenses/LICENSE-2.0>.
 */

/**
 * Implementations of Hibernate Validator specific constraints.
 */
package com.arm256.validate.constraintvalidators.hv;
