
package com.arm256.validate.constraintvalidators.bv.number.sign;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.bv.number.InfinityNumberComparatorHelper;
import com.arm256.validator.AbstractConstraintValidator;

@Service("PositiveString")
public class PositiveValidatorForCharSequence extends AbstractConstraintValidator<CharSequence> {

	@Override
	public boolean isValid(CharSequence value, int context) {
		// null values are valid
		if ( value == null ) {
			return false;
		}

		try {
			return NumberSignHelper.signum(new BigDecimal(value.toString()),
					InfinityNumberComparatorHelper.LESS_THAN) > 0;
		}
		catch (NumberFormatException nfe) {
			return false;
		}
	}
}
