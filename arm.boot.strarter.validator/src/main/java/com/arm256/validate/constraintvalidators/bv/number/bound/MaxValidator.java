
package com.arm256.validate.constraintvalidators.bv.number.bound;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.MaxConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@Service("Max")
public class MaxValidator extends AbstractConstraintValidator<Number> {

	public MaxValidator(MaxConstraint p) {
		super(p);
	}

	@Override
	public boolean isValid(Number value, int context) {
		// null values are valid
		if ( value == null ) {
			return false;
		}

		long max = getParams(context, 0);

		return NumberComparatorHelper.compare(value, max, NumberComparatorHelper.GREATER_THAN) <= 0;
	}

}
