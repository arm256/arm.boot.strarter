
package com.arm256.validate.constraintvalidators.bv.notempty;

import org.springframework.stereotype.Service;

import com.arm256.validator.AbstractConstraintValidator;


@Service("NotEmptyString")
public class NotEmptyValidatorForCharSequence extends AbstractConstraintValidator<CharSequence> {

	/**
	 * Checks the character sequence is not {@code null} and not empty.
	 *
	 * @param charSequence the character sequence to validate
	 * @param constraintValidatorContext context in which the constraint is evaluated
	 * @return returns {@code true} if the character sequence is not {@code null} and not empty.
	 */
	@Override
	public boolean isValid(CharSequence charSequence, int context) {
		if ( charSequence == null ) {
			return false;
		}
		return charSequence.length() > 0;
	}
}
