
package com.arm256.validate.constraintvalidators.bv.size;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.SizeConstraint;

@Service("SizeArrayInt")
public class SizeValidatorForArraysOfInt extends SizeValidatorForArray<Integer> {

	public SizeValidatorForArraysOfInt(SizeConstraint p) {
		super(p);
	}

}