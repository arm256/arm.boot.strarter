package com.arm256.validate.constraintvalidators.bv;

import org.springframework.stereotype.Service;

import com.arm256.validator.AbstractConstraintValidator;

@Service("NotNull")
public class NotNullValidator extends AbstractConstraintValidator<Object> {

	@Override
	public boolean isValid(Object object, int context) {
		return object != null;
	}
}
