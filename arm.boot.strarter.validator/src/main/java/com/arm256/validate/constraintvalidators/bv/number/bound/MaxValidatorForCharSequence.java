
package com.arm256.validate.constraintvalidators.bv.number.bound;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.MaxConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@Service("MaxString")
public class MaxValidatorForCharSequence extends AbstractConstraintValidator<CharSequence> {

	public MaxValidatorForCharSequence(MaxConstraint p) {
		super(p);
	}

	@Override
	public boolean isValid(CharSequence value, int context) {
		// null values are valid
		if (value == null) {
			return false;
		}

		long max = getParams(context, 0);

		try {
			return NumberComparatorHelper.compare(new BigDecimal(value.toString()), max,
					NumberComparatorHelper.GREATER_THAN) <= 0;
		}
		catch (NumberFormatException nfe) {
			return false;
		}
	}
}
