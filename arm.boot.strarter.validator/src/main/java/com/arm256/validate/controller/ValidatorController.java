package com.arm256.validate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arm256.annotation.SwaggerController;
import com.arm256.validator.Validators;
import com.arm256.validator.ValidatorsService;

@RestController
@RequestMapping(ValidatorController.path)
@SwaggerController
public class ValidatorController {
	public final static String path = "v1/validate";

	@Autowired
	private ValidatorsService validators;

	@GetMapping()
	public List<Validators> readValidators() {
		return validators.readValidators();
	}
}
