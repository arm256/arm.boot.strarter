
package com.arm256.validate.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.arm256.validate.controller.ValidatorController;
import com.arm256.validate.service.ValidatorsServiceImpl;

@Configuration
@Import({ ValidatorController.class, 
		ValidatorsServiceImpl.class })
@ComponentScan("com.arm256.validate.constraintvalidators")
public class ValidatorServerConfiguration {

//	@Bean
//	public static BeanFactoryPostProcessor entityScanPackagesPostProcessor() {
//		return beanFactory -> {
//			if (beanFactory instanceof BeanDefinitionRegistry) {
//				EntityScanPackages.register((BeanDefinitionRegistry) beanFactory,
//						Collections.singletonList(ValidatorEntity.class.getPackage().getName()));
//			}
//		};
//	}



}
