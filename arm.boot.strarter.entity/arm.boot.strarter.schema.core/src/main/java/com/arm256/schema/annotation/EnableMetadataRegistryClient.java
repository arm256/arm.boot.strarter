
package com.arm256.schema.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.arm256.annotation.EnableStoreServices;
import com.arm256.schema.config.MetadataRegistryClientConfiguration;
import com.arm256.validate.annotation.EnableValidatorServer;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@EnableStoreServices
@EnableValidatorServer
@Import(MetadataRegistryClientConfiguration.class)
public @interface EnableMetadataRegistryClient {

}
