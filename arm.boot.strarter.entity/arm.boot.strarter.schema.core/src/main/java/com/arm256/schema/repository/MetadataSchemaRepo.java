package com.arm256.schema.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.arm256.entity.metadata.MetadataSchemaEntity;
import com.arm256.repository.GenericLong;

@Repository
public interface MetadataSchemaRepo extends GenericLong<MetadataSchemaEntity> {
	public Optional<MetadataSchemaEntity> findByName(String name);
}
