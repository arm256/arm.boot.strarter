package com.arm256.schema.model.mapper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.arm256.entity.metadata.RequestInfoEntity;
import com.arm256.model.RequestInfo;
import com.arm256.model.mapper.AbstractMapper;

@Component
@Qualifier("request_info-mapper")
public class RequestInfoMapper extends AbstractMapper<RequestInfoEntity, RequestInfo> {

	public RequestInfoMapper() {
		super(RequestInfoEntity.class, RequestInfo.class);
	}

	@Override
	public RequestInfoEntity entityUpdate(RequestInfoEntity entity, RequestInfo dto) {
		return entity;
	}


}
