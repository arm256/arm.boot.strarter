package com.arm256.schema.service;

import java.util.List;

import com.arm256.entity.metadata.MetadataMapEntity;
import com.arm256.entity.metadata.MetadataSchemaEntity;
import com.arm256.model.MetadataMap;
import com.arm256.service.CRUD_LONG;

public interface MetadataMapService extends CRUD_LONG<MetadataMapEntity, MetadataMap> {

	public List<MetadataMapEntity> read(MetadataSchemaEntity schema);

}
