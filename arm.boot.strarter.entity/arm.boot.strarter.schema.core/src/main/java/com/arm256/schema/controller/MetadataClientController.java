package com.arm256.schema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arm256.annotation.SwaggerController;
import com.arm256.schema.client.SchemaClient;

@RestController
@RequestMapping(MetadataClientController.path)
@SwaggerController
public class MetadataClientController {
	public final static String path = "v1/metadata/refresh";

	@Autowired
	private SchemaClient schemaCient;

	@GetMapping("/")
	public void refreshAll() {
		schemaCient.refreshAll();
	}

	@GetMapping("/fields")
	public void refreshFields() {
		schemaCient.refreshFields();
	}

	@GetMapping("/maps")
	public void refreshMaps() {
		schemaCient.refreshMaps();
	}

	@GetMapping("/requestInfo")
	public void refreshRequestInfos() {
		schemaCient.refreshRequestInfos();
	}


}
