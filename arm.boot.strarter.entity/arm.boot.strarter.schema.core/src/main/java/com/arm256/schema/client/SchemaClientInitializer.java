package com.arm256.schema.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

public class SchemaClientInitializer implements CommandLineRunner {

	@Autowired
	private SchemaClient schemaCient;

	@Override
	public void run(String... args) throws Exception {
		schemaCient.refreshAll();
	}

}
