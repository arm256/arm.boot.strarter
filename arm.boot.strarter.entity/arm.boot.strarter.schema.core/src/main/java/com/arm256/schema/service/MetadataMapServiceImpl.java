package com.arm256.schema.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.arm256.entity.metadata.MetadataFieldEntity;
import com.arm256.entity.metadata.MetadataMapEntity;
import com.arm256.entity.metadata.MetadataSchemaEntity;
import com.arm256.model.MetadataMap;
import com.arm256.model.mapper.Mapper;
import com.arm256.schema.repository.MetadataMapRepo;
import com.arm256.service.AbstractServiceImplLong;
import com.arm256.service.StoreMetadataMapService;

@Service
public class MetadataMapServiceImpl extends
		AbstractServiceImplLong<MetadataMapEntity, MetadataMap, MetadataMapRepo> implements MetadataMapService {

	@Autowired
	private MetadataFieldService fieldService;

	@Autowired
	private StoreMetadataMapService metadataMapService;

	@Autowired
	public MetadataMapServiceImpl(MetadataMapRepo repository,
			@Qualifier("metadata_map-mapper") Mapper<MetadataMapEntity, MetadataMap> mapper) {
		super(repository, mapper);
	}

	@Override
	public MetadataMap create(MetadataMap dto) {
		MetadataFieldEntity fieldEntity = fieldService.readByPathEntity(dto.getMetadataField().getElementPath());

		MetadataMapEntity mapEntity = getMapper().toEntity(dto);

		mapEntity.setMetadataSchema(fieldEntity.getMetadataSchema());
		mapEntity.setMetadataField(fieldEntity);

		dto = getMapper().toDto(super.create(mapEntity));

		metadataMapService.add(fieldEntity.getMetadataSchema().getName(), dto);

		return dto;
	}

	@Override
	public String name() {
		return "metaData Map";
	}

	@Override
	public List<MetadataMapEntity> read(MetadataSchemaEntity schema) {
		return repository.findByMetadataSchema(schema);
	}


}
