package com.arm256.schema.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.arm256.model.MetadataField;
import com.arm256.model.MetadataMap;
import com.arm256.model.RequestInfo;
import com.arm256.schema.controller.MetadataFieldController;
import com.arm256.schema.controller.MetadataMapController;
import com.arm256.schema.controller.RequestInfoController;

@FeignClient(name = "schemaClient", url = "${arm.schema.url}")
public interface SchemaClientProxy {
	
	@GetMapping(MetadataFieldController.path)
	public List<MetadataField> readFields(@RequestParam("page") int page, @RequestParam("size") int size);

	@GetMapping(MetadataMapController.path)
	public List<MetadataMap> readMaps(@RequestParam("page") int page, @RequestParam("size") int size);

	@GetMapping(RequestInfoController.path)
	public List<RequestInfo> readRequestInfos(@RequestParam("page") int page,
			@RequestParam("size") int size);


}
