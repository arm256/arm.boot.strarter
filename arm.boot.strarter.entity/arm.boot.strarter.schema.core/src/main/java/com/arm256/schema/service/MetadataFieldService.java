package com.arm256.schema.service;

import java.util.List;

import com.arm256.entity.metadata.MetadataFieldEntity;
import com.arm256.model.FieldValidators;
import com.arm256.model.MetadataField;
import com.arm256.service.CRUD_LONG;

public interface MetadataFieldService extends CRUD_LONG<MetadataFieldEntity, MetadataField> {

	public MetadataField readByPath(String mdString);

	public MetadataFieldEntity readByPathEntity(String mdString);

	public MetadataField read(String metadataSchema, String element, String qualifier);

	public List<MetadataField> read(String metadataSchema, String element);

	public List<MetadataField> read(String metadataSchema);

	public MetadataField addValitators(long id, List<FieldValidators> validatores);

}
