package com.arm256.schema.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arm256.annotation.SwaggerController;
import com.arm256.controller.AbstractCRUDController;
import com.arm256.model.MetadataMap;
import com.arm256.schema.service.MetadataMapService;

@RestController
@RequestMapping(MetadataMapController.path)
@SwaggerController
public class MetadataMapController extends AbstractCRUDController<MetadataMap, Long> {
	public final static String path = "v1/metadata/map";

	public MetadataMapController(MetadataMapService service) {
		super(service);
	}

	@Override
	public String controllerPath() {
		return path;
	}

}
