
package com.arm256.schema.config;

import java.util.Collections;

import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.domain.EntityScanPackages;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.arm256.entity.ArmObjectEntity;
import com.arm256.entity.metadata.MetadataValueEntity;
import com.arm256.model.mapper.metadata.MetadataValueMapper;
import com.arm256.schema.model.mapper.MetadataFieldMapper;
import com.arm256.schema.model.mapper.MetadataMapMapper;
import com.arm256.schema.model.mapper.MetadataSchemaMapper;
import com.arm256.schema.model.mapper.MetadataValidatorMapper;
import com.arm256.schema.model.mapper.MetadataValidatorParamMapper;
import com.arm256.schema.model.mapper.RequestInfoMapper;
import com.arm256.schema.repository.MetadataFieldRepo;
import com.arm256.schema.repository.MetadataMapRepo;
import com.arm256.schema.repository.MetadataSchemaRepo;
import com.arm256.schema.repository.MetadataValueRepo;
import com.arm256.schema.repository.RequestInfoRepo;
import com.arm256.schema.service.MetadataFieldServiceImpl;
import com.arm256.schema.service.MetadataMapServiceImpl;
import com.arm256.schema.service.MetadataSchemaServiceImpl;
import com.arm256.schema.service.MetadataValueServiceImpl;
import com.arm256.schema.service.RequestInfoServiceImpl;


@Configuration
@EnableJpaRepositories(basePackageClasses = { MetadataFieldRepo.class, MetadataSchemaRepo.class,
		MetadataValueRepo.class, MetadataMapRepo.class, RequestInfoRepo.class })
@EnableConfigurationProperties(SchemaServerProperties.class)
@EntityScan(basePackageClasses = ArmObjectEntity.class)
@Import({ 
		MetadataFieldServiceImpl.class, MetadataSchemaServiceImpl.class, MetadataValueServiceImpl.class,
		MetadataMapServiceImpl.class, RequestInfoServiceImpl.class, MetadataFieldMapper.class,
		MetadataSchemaMapper.class, MetadataValueMapper.class,
		MetadataMapMapper.class, MetadataValidatorMapper.class, MetadataValidatorParamMapper.class ,RequestInfoMapper.class})
public class MetadataRegistryServiceConfiguration {

	@Bean
	public static BeanFactoryPostProcessor entityScanPackagesPostProcessor() {
		return beanFactory -> {
			if (beanFactory instanceof BeanDefinitionRegistry) {
				EntityScanPackages.register((BeanDefinitionRegistry) beanFactory,
						Collections.singletonList(MetadataValueEntity.class.getPackage().getName()));
			}
		};
	}


}
