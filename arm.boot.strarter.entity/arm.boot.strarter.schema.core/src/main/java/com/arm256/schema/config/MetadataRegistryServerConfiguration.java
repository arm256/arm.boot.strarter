
package com.arm256.schema.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.arm256.schema.controller.MetadataFieldController;
import com.arm256.schema.controller.MetadataMapController;
import com.arm256.schema.controller.MetadataSchemaController;
import com.arm256.schema.controller.RequestInfoController;


@Configuration
@Import({ MetadataFieldController.class, MetadataSchemaController.class, MetadataMapController.class,
		RequestInfoController.class })
public class MetadataRegistryServerConfiguration {

}
