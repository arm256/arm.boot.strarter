package com.arm256.schema.model.mapper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.arm256.entity.metadata.ParamsValueEntity;
import com.arm256.model.ParamValues;
import com.arm256.model.mapper.AbstractMapperList;

@Component
@Qualifier("metadata_validator-mapper")
public class MetadataValidatorParamMapper extends AbstractMapperList<ParamsValueEntity, ParamValues> {

	public MetadataValidatorParamMapper() {
		super(ParamsValueEntity.class, ParamValues.class);
	}


	@Override
	public ParamsValueEntity entityUpdate(ParamsValueEntity entity, ParamValues dto) {
		entity.setParam(dto.getParam());
		entity.setValue(dto.getValue());
		return entity;
	}

}
