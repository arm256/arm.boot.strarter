package com.arm256.schema.model.mapper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.arm256.entity.metadata.MetadataSchemaEntity;
import com.arm256.model.MetadataSchema;
import com.arm256.model.mapper.AbstractMapper;

@Component
@Qualifier("metadata_schema-mapper")
public class MetadataSchemaMapper extends AbstractMapper<MetadataSchemaEntity, MetadataSchema> {

	public MetadataSchemaMapper() {
		super(MetadataSchemaEntity.class, MetadataSchema.class);
	}

	@Override
	public MetadataSchemaEntity entityUpdate(MetadataSchemaEntity entity, MetadataSchema dto) {
		entity.setName(dto.getName());
		entity.setNamespace(dto.getNamespace());
		return entity;
	}


}
