package com.arm256.schema.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.arm256.entity.metadata.MetadataFieldEntity;
import com.arm256.entity.metadata.MetadataFieldValidatorsEntity;
import com.arm256.entity.metadata.MetadataSchemaEntity;
import com.arm256.entity.metadata.ParamsValueEntity;
import com.arm256.model.FieldValidators;
import com.arm256.model.MetadataField;
import com.arm256.model.mapper.Mapper;
import com.arm256.schema.repository.MetadataFieldRepo;
import com.arm256.service.AbstractServiceImplLong;
import com.arm256.service.StoreMetadataFieldService;
import com.arm256.utils.CoreUtils;
import com.arm256.utils.Pair;
import com.arm256.utils.SupportClazz;
import com.arm256.validator.ValidatorsService;
import com.jnape.palatable.lambda.adt.hlist.Tuple3;

@Service
public class MetadataFieldServiceImpl extends
		AbstractServiceImplLong<MetadataFieldEntity, MetadataField, MetadataFieldRepo> implements MetadataFieldService {
	@Autowired
	private MetadataSchemaService schemaService;

	@Autowired
	private ValidatorsService vService;

	@Autowired
	private StoreMetadataFieldService metadataFieldStore;

	@Autowired
	public MetadataFieldServiceImpl(MetadataFieldRepo repository,
			@Qualifier("metadata_field-mapper") Mapper<MetadataFieldEntity, MetadataField> mapper) {
		super(repository, mapper);
	}

	@Override
	public MetadataField create(MetadataField dto) {
		MetadataSchemaEntity schemaEntity = schemaService.read(dto.getSchema());

		if (!SupportClazz.isClassSupported(dto.getClazzType())) {
			throw notFound.get();
		}
		MetadataFieldEntity fieldEntity = getMapper().toEntity(dto);
		fieldEntity.setMetadataSchema(schemaEntity);
		MetadataField field = getMapper().toDto(super.create(fieldEntity));
		metadataFieldStore.add(schemaEntity.getName(), field);
		return field;
	}

	@Override
	public String name() {
		return "metaData field";
	}

	@Override
	public MetadataFieldEntity readByPathEntity(String mdString) {
		Tuple3<String, String, String> sch = CoreUtils.readSchema(mdString);

		MetadataSchemaEntity schemaEntity = schemaService.read(sch._1());

		return read(schemaEntity, sch._2(), sch._3());
	}

	@Override
	public MetadataField readByPath(String mdString) {
		return toDto(readByPathEntity(mdString));
	}

	@Override
	public MetadataField read(String metadataSchema, String element, String qualifier) {
		MetadataSchemaEntity schemaEntity = schemaService.read(metadataSchema);
		return toDto(read(schemaEntity, element, qualifier));
	}

	private MetadataFieldEntity read(MetadataSchemaEntity metadataSchema, String element, String qualifier) {
		return repository.findByMetadataSchemaAndElementAndQualifier(metadataSchema, element, qualifier)
				.orElseThrow(notFound);
	}

	@Override
	public List<MetadataField> read(String metadataSchema, String element) {
		MetadataSchemaEntity schemaEntity = schemaService.read(metadataSchema);
		return toDto(notEmpty(repository.findByMetadataSchemaAndElement(schemaEntity, element)));
	}

	@Override
	public List<MetadataField> read(String metadataSchema) {
		MetadataSchemaEntity schemaEntity = schemaService.read(metadataSchema);
		return toDto(notEmpty(repository.findByMetadataSchema(schemaEntity)));
	}

	@Override
	public MetadataField addValitators(long id, List<FieldValidators> validatores) {
		MetadataFieldEntity field = read(() -> id);

		for (FieldValidators fv : validatores) {

			vService.addValidators(fv.getName(), fv.readParams(), field.getClazzType());

			MetadataFieldValidatorsEntity validatorEntity = MetadataFieldValidatorsEntity.builder().name(fv.getName())
					.fieldObject(field).build();
			if (fv.getParams() != null) {
				for (Pair<String, String> pair : fv.readParams()) {
					validatorEntity.getParams().add(ParamsValueEntity.builder().param(pair.getKey())
							.value(pair.getValues()).fieldValidator(validatorEntity).build());
				}
			}

			field.getValidators().add(validatorEntity);

		}

		MetadataField f = mapper.toDto(create(field));
		metadataFieldStore.add(f.getSchema(), f);
		return f;

	}



}
