
package com.arm256.schema.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.arm256.annotation.EnableJpa;
import com.arm256.annotation.EnableSwaggerMvc;
import com.arm256.schema.config.MetadataRegistryServiceConfiguration;
import com.arm256.validate.annotation.EnableValidatorServer;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@EnableValidatorServer
@EnableSwaggerMvc
@EnableJpa
@Import(MetadataRegistryServiceConfiguration.class)
public @interface EnableMetadataRegistryService {

}
