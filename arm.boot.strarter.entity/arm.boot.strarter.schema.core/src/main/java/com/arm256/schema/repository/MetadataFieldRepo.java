package com.arm256.schema.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.arm256.entity.metadata.MetadataFieldEntity;
import com.arm256.entity.metadata.MetadataSchemaEntity;
import com.arm256.repository.GenericLong;

@Repository
public interface MetadataFieldRepo extends GenericLong<MetadataFieldEntity> {

	public Optional<MetadataFieldEntity> findByMetadataSchemaAndElementAndQualifier(MetadataSchemaEntity metadataSchema,
			String element, String qualifier);

	public List<MetadataFieldEntity> findByMetadataSchemaAndElement(MetadataSchemaEntity metadataSchema,
			String element);

	public List<MetadataFieldEntity> findByMetadataSchema(MetadataSchemaEntity metadataSchema);
}
