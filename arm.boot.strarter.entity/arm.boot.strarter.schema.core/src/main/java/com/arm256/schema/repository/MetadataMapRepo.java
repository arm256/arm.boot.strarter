package com.arm256.schema.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.arm256.entity.metadata.MetadataMapEntity;
import com.arm256.entity.metadata.MetadataSchemaEntity;
import com.arm256.repository.GenericLong;

@Repository
public interface MetadataMapRepo extends GenericLong<MetadataMapEntity> {

	public List<MetadataMapEntity> findByMetadataSchema(MetadataSchemaEntity metadataSchema);

}
