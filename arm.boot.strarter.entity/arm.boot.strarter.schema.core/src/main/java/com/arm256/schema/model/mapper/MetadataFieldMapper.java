package com.arm256.schema.model.mapper;

import javax.annotation.PostConstruct;

import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.arm256.entity.metadata.MetadataFieldEntity;
import com.arm256.model.MetadataField;
import com.arm256.model.mapper.AbstractMapper;

@Component
@Qualifier("metadata_field-mapper")
public class MetadataFieldMapper extends AbstractMapper<MetadataFieldEntity, MetadataField> {

	@Autowired
	private MetadataValidatorMapper validator;

	public MetadataFieldMapper() {
		super(MetadataFieldEntity.class, MetadataField.class);
	}

	@Override
	public MetadataFieldEntity entityUpdate(MetadataFieldEntity entity, MetadataField dto) {
		entity.setElement(dto.getElement());
		entity.setQualifier(dto.getQualifier());
		entity.setScopeNote(dto.getScopeNote());
		return entity;
	}

	@PostConstruct
	public void init() {

		modelMapper.addMappings(new PropertyMap<MetadataFieldEntity, MetadataField>() {

			@Override
			protected void configure() {
				map().setSchema(source.getMetadataSchema().getName());
				using(validator).map(source.getValidators()).setValidators(null);
			}
		});
	}


}
