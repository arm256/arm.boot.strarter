
package com.arm256.schema.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.arm256.schema.client.SchemaClient;
import com.arm256.schema.client.SchemaClientInitializer;
import com.arm256.schema.client.SchemaClientProxy;
import com.arm256.schema.controller.MetadataClientController;


@Configuration
@EnableFeignClients(clients = { SchemaClientProxy.class })
@Import({ SchemaClient.class, MetadataClientController.class, SchemaClientInitializer.class })
public class MetadataRegistryClientConfiguration {

}
