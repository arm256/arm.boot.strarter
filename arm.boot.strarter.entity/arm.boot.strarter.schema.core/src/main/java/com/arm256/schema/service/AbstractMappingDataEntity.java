package com.arm256.schema.service;

import java.util.List;
import java.util.function.BiConsumer;

import org.springframework.beans.factory.annotation.Autowired;

import com.arm256.entity.ArmObjectEntity;
import com.arm256.model.ArmObject;
import com.arm256.model.MetadataField;
import com.arm256.model.MetadataMap;
import com.arm256.model.mapper.Mapper;
import com.arm256.repository.GenericArm;
import com.arm256.service.AbstractServiceImplARM;
import com.arm256.service.MappingData;
import com.arm256.service.StoreMetadataFieldService;
import com.arm256.service.StoreMetadataMapService;
import com.arm256.validate.service.ValidatorsServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;

public abstract class AbstractMappingDataEntity<E extends ArmObjectEntity, D extends ArmObject, R extends GenericArm<E>>
		extends AbstractServiceImplARM<E, D, R> implements MappingData {

	public AbstractMappingDataEntity(R repository, Mapper<E, D> mapper) {
		super(repository, mapper);
	}

	@Autowired
	private ValidatorsServiceImpl validatorService;

	@Autowired
	private StoreMetadataMapService mapService;

	@Autowired
	private StoreMetadataFieldService fieldService;

	@Override
	public List<MetadataMap> readMetadataMap(String schema) {
		return mapService.read(schema);
	}

	@Override
	public List<MetadataField> readMetadataField(String schema) {
		return fieldService.read(schema);
	}

	public D mapingDataValues(JsonNode json, D d) {

		return d;

	}

	@Override
	public BiConsumer<List<String>, MetadataField> validate() {
		return (values, field) -> {
			if (List.class.isAssignableFrom(field.getClazzType())) {
				validatorService.validate(values, field);
			} else {
				validatorService.validate(values.get(0), field);
			}
		};
	}


	public abstract String mainSchema();

}
