package com.arm256.schema.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arm256.model.MetadataField;
import com.arm256.model.MetadataMap;
import com.arm256.model.RequestInfo;
import com.arm256.service.StoreMetadataFieldService;
import com.arm256.service.StoreMetadataMapService;
import com.arm256.service.StoreRequestInfoService;

@Component
public class SchemaClient {

	@Autowired
	private SchemaClientProxy schemaClient;

	@Autowired
	private StoreMetadataFieldService metadataFieldStore;

	@Autowired
	private StoreMetadataMapService metadataMapStore;

	@Autowired
	private StoreRequestInfoService requestInfoStore;

	public void refreshAll() {

		refreshFields();
		refreshMaps();
		refreshRequestInfos();
	}
	public void refreshFields() {
		metadataFieldStore.clearAll();
		List<MetadataField> fields = schemaClient.readFields(0, Integer.MAX_VALUE);
		fields.forEach(e -> {
			metadataFieldStore.add(e.getSchema(), e);
		});
	}

	public void refreshMaps() {
		metadataFieldStore.clearAll();
		List<MetadataMap> maps = schemaClient.readMaps(0, Integer.MAX_VALUE);
		maps.forEach(e -> {
			metadataMapStore.add(e.getSchema(), e);
		});
	}

	public void refreshRequestInfos() {
		metadataFieldStore.clearAll();
		List<RequestInfo> requestInfos = schemaClient.readRequestInfos(0, Integer.MAX_VALUE);
		requestInfos.forEach(e -> {
			requestInfoStore.add(e.getIdentity(), e);
		});
	}
}
