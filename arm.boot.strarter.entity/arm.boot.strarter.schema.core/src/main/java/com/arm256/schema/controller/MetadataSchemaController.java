package com.arm256.schema.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arm256.annotation.SwaggerController;
import com.arm256.controller.AbstractCRUDController;
import com.arm256.model.MetadataSchema;
import com.arm256.schema.service.MetadataSchemaService;

@RestController
@RequestMapping(MetadataSchemaController.path)
@SwaggerController
public class MetadataSchemaController extends AbstractCRUDController<MetadataSchema, Long> {
	public final static String path = "v1/metadata/schema";

	public MetadataSchemaController(MetadataSchemaService service) {
		super(service);
	}

	@Override
	public String controllerPath() {
		return path;
	}


}
