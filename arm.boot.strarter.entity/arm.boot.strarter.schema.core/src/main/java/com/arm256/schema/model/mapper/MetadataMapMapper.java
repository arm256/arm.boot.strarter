package com.arm256.schema.model.mapper;

import javax.annotation.PostConstruct;

import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.arm256.entity.metadata.MetadataMapEntity;
import com.arm256.model.MetadataMap;
import com.arm256.model.mapper.AbstractMapper;

@Component
@Qualifier("metadata_map-mapper")
public class MetadataMapMapper extends AbstractMapper<MetadataMapEntity, MetadataMap> {

	public MetadataMapMapper() {
		super(MetadataMapEntity.class, MetadataMap.class);
	}

	@Override
	public MetadataMapEntity entityUpdate(MetadataMapEntity entity, MetadataMap dto) {
		entity.setMapField(dto.getMapField());
		entity.setPointer(dto.getPointer());
		return entity;
	}

	@PostConstruct
	public void init() {

		modelMapper.addMappings(new PropertyMap<MetadataMapEntity, MetadataMap>() {

			@Override
			protected void configure() {
				map().setSchema(source.getMetadataSchema().getName());
			}
		});
	}

}
