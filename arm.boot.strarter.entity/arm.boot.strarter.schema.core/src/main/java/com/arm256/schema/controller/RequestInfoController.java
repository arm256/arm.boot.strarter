package com.arm256.schema.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arm256.annotation.SwaggerController;
import com.arm256.controller.AbstractCRUDController;
import com.arm256.model.RequestInfo;
import com.arm256.schema.service.RequestInfoService;

@RestController
@RequestMapping(RequestInfoController.path)
@SwaggerController
public class RequestInfoController extends AbstractCRUDController<RequestInfo, Long> {
	public final static String path = "v1/metadata/requests";

	public RequestInfoController(RequestInfoService service) {
		super(service);
	}

	@Override
	public String controllerPath() {
		return path;
	}


}
