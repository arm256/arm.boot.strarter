package com.arm256.schema.service;

import com.arm256.entity.metadata.RequestInfoEntity;
import com.arm256.model.RequestInfo;
import com.arm256.service.CRUD_LONG;

public interface RequestInfoService extends CRUD_LONG<RequestInfoEntity, RequestInfo> {
}
