package com.arm256.schema.repository;

import org.springframework.stereotype.Repository;

import com.arm256.entity.metadata.MetadataValueEntity;
import com.arm256.repository.GenericLong;

@Repository
public interface MetadataValueRepo extends GenericLong<MetadataValueEntity> {

}
