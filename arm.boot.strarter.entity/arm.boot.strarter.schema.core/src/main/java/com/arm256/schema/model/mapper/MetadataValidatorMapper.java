package com.arm256.schema.model.mapper;

import javax.annotation.PostConstruct;

import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.arm256.entity.metadata.MetadataFieldValidatorsEntity;
import com.arm256.model.FieldValidators;
import com.arm256.model.mapper.AbstractMapperList;

@Component
@Qualifier("metadata_validator-mapper")
public class MetadataValidatorMapper extends AbstractMapperList<MetadataFieldValidatorsEntity, FieldValidators> {

	@Autowired
	private MetadataValidatorParamMapper validatorParam;

	public MetadataValidatorMapper() {
		super(MetadataFieldValidatorsEntity.class, FieldValidators.class);
	}

	@Override
	public MetadataFieldValidatorsEntity entityUpdate(MetadataFieldValidatorsEntity entity, FieldValidators dto) {
		return entity;
	}

	@PostConstruct
	public void init() {

		modelMapper.addMappings(new PropertyMap<MetadataFieldValidatorsEntity, FieldValidators>() {

			@Override
			protected void configure() {

				using(validatorParam).map(source.getParams()).setParams(null);
			}
		});
	}

}
