package com.arm256.schema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.arm256.entity.metadata.MetadataSchemaEntity;
import com.arm256.exception.NotFoundException;
import com.arm256.model.MetadataSchema;
import com.arm256.model.mapper.Mapper;
import com.arm256.schema.repository.MetadataSchemaRepo;
import com.arm256.service.AbstractServiceImplLong;

@Service
public class MetadataSchemaServiceImpl extends
		AbstractServiceImplLong<MetadataSchemaEntity, MetadataSchema, MetadataSchemaRepo>
		implements MetadataSchemaService {
	@Autowired
	public MetadataSchemaServiceImpl(MetadataSchemaRepo repository,
			@Qualifier("metadata_schema-mapper") Mapper<MetadataSchemaEntity, MetadataSchema> mapper) {
		super(repository, mapper);
	}

	@Override
	public MetadataSchemaEntity read(String name) {
		return repository.findByName(name).orElseThrow(() -> new NotFoundException(name()));
	}

	@Override
	public String name() {
		return "metaData schema";
	}



}
