package com.arm256.schema.service;

import com.arm256.entity.metadata.MetadataSchemaEntity;
import com.arm256.model.MetadataSchema;
import com.arm256.service.CRUD_LONG;

public interface MetadataSchemaService extends CRUD_LONG<MetadataSchemaEntity, MetadataSchema> {
	public MetadataSchemaEntity read(String name);
}
