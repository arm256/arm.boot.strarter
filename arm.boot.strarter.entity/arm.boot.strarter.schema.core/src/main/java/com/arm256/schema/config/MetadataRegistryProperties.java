package com.arm256.schema.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.var;

@Getter
@Setter
@ToString
@Component
public class MetadataRegistryProperties {

	@Autowired
	private ApplicationContext context;

	@Autowired
	private Environment environment;


	private String name;
	private String version;
	private String controllerPackage;

	@PostConstruct
	public void init() {

		initControllerPackageName();
		initVersion();
		initAppName();
	}

	private void initControllerPackageName() {
		controllerPackage = environment.getProperty("arm.swagger.controllers-package");
		if (controllerPackage == null || controllerPackage.isEmpty()) {
			controllerPackage = context.getBeansWithAnnotation(SpringBootApplication.class).values().stream()
					.findFirst().map(Object::getClass).map(e -> e.getPackage().getName()).orElse("");
		}
	}

	private void initAppName() {

		name = environment.getProperty("arm.swagger.name");
		if (name == null || name.isEmpty()) {
			name = environment.getProperty("spring.application.name", "");
		}

		if (name == null || name.isEmpty()) {

			String mainClassName = context.getBeansWithAnnotation(SpringBootApplication.class).values().stream()
					.findFirst().map(Object::getClass).map(Class::getSimpleName).orElse("");

			name = getAppNameFromAppMainClassName(mainClassName);
		}
	}

	private String getAppNameFromAppMainClassName(String mainClassName) {
		if (mainClassName.contains("Application")) {
			mainClassName = mainClassName.substring(0, mainClassName.lastIndexOf("Application"));
		}
		var nameBuilder = new StringBuilder();

		for (char letter : mainClassName.toCharArray()) {
			if (Character.isUpperCase(letter) && nameBuilder.length() != 0) {
				nameBuilder.append("-");
			}
			nameBuilder.append(Character.toLowerCase(letter));
		}
		return nameBuilder.toString();
	}

	private void initVersion() {

		version = environment.getProperty("spring.application.version", "version - 1.0");
	}
}
