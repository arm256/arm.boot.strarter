package com.arm256.schema.service;

import com.arm256.entity.metadata.MetadataValueEntity;
import com.arm256.model.MetadataValue;
import com.arm256.service.CRUD_LONG;

public interface MetadataValueService extends CRUD_LONG<MetadataValueEntity, MetadataValue> {

}
