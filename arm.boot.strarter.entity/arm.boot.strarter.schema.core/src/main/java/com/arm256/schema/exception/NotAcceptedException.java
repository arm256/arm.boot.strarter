package com.arm256.schema.exception;

import com.arm256.exception.AbstractArmException;

@SuppressWarnings("serial")
public class NotAcceptedException extends AbstractArmException {

	public NotAcceptedException() {
		super("BUS_EXE_1", "entity.notAccept");
	}

}
