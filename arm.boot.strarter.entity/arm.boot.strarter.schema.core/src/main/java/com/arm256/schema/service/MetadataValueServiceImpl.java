package com.arm256.schema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.arm256.entity.metadata.MetadataValueEntity;
import com.arm256.model.MetadataValue;
import com.arm256.model.mapper.Mapper;
import com.arm256.schema.repository.MetadataValueRepo;
import com.arm256.service.AbstractServiceImplLong;

@Service
public class MetadataValueServiceImpl extends
		AbstractServiceImplLong<MetadataValueEntity, MetadataValue, MetadataValueRepo>
		implements MetadataValueService {

	@Autowired
	public MetadataValueServiceImpl(MetadataValueRepo repository,
			@Qualifier("metadata_value-mapper") Mapper<MetadataValueEntity, MetadataValue> mapper) {
		super(repository, mapper);
	}

	@Override
	public String name() {
		return "metaData value";
	}


}
