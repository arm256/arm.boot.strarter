package com.arm256.schema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.arm256.entity.metadata.RequestInfoEntity;
import com.arm256.model.RequestInfo;
import com.arm256.model.mapper.Mapper;
import com.arm256.schema.repository.RequestInfoRepo;
import com.arm256.service.AbstractServiceImplLong;
import com.arm256.service.StoreRequestInfoService;

@Service
public class RequestInfoServiceImpl extends
		AbstractServiceImplLong<RequestInfoEntity, RequestInfo, RequestInfoRepo>
		implements RequestInfoService {

	@Autowired
	private StoreRequestInfoService requestInfostore;

	@Autowired
	public RequestInfoServiceImpl(RequestInfoRepo repository,
			@Qualifier("request_info-mapper") Mapper<RequestInfoEntity, RequestInfo> mapper) {
		super(repository, mapper);
	}

	@Override
	public RequestInfo create(RequestInfo d) {
		d = super.create(d);
		requestInfostore.add(d.getIdentity(), d);
		return d;
	}

	@Override
	public String name() {
		return "Request Info";
	}



}
