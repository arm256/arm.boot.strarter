package com.arm256.schema.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.arm256.annotation.SwaggerController;
import com.arm256.controller.AbstractCRUDController;
import com.arm256.model.FieldValidators;
import com.arm256.model.MetadataField;
import com.arm256.schema.service.MetadataFieldService;

@RestController
@RequestMapping(MetadataFieldController.path)
@SwaggerController
public class MetadataFieldController extends AbstractCRUDController<MetadataField, Long> {
	public final static String path = "v1/metadata/field";

	public MetadataFieldController(MetadataFieldService service) {
		super(service);
	}

	@PostMapping("/{id}/validators")
	@ResponseStatus(HttpStatus.CREATED)
	public MetadataField addValitators(@PathVariable("id") long id, @RequestBody List<FieldValidators> validatores) {
		return ((MetadataFieldService) service).addValitators(id, validatores);
	}

	@Override
	public String controllerPath() {
		return path;
	}

	@GetMapping("/schema/{name}")
	public List<MetadataField> readFields(@PathVariable("id") String metadataSchema) {
		return ((MetadataFieldService) service).read(metadataSchema);
	}

}
