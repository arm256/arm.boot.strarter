package com.arm256.exception;

import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.codec.DecodingException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ServerWebInputException;

import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class RestExceptionHandler {

//	private final MessageSource messageSource;
//
//	@Autowired
//	public RestExceptionHandler(MessageSource messageSource) {
//		this.messageSource = messageSource;
//	}

	@ExceptionHandler(value = { NotFoundException.class })
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<Void> notFountId(NotFoundException ex) {
		return ResponseEntity.notFound().build();
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = { InvalidDataException.class })
	public ResponseEntity<Void> invalidData(InvalidDataException e) {
		return ResponseEntity.badRequest().header("X-error-message", e.getMessage()).header("X-error-code", e.getCode())
				.build();
	}



	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = { MissingParamsException.class })
	public ResponseEntity<Void> missingParams(MissingParamsException e) {
		return ResponseEntity.badRequest().header("X-error-message", "required param missing")
				.header("X-error-code", e.getCode()).build();
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = { ServerWebInputException.class })
	public ResponseEntity<Void> invalidFormatDecodingException(ServerWebInputException e) {
		return ResponseEntity.badRequest().header("X-error-message", e.getMessage()).header("X-error-code", "SYS_EXE_2")
				.build();
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = { JsonException.class })
	public ResponseEntity<Void> jsonProcessingException(JsonException e) {
		return ResponseEntity.badRequest().header("X-error-message", "Bad Json").header("X-error-code", e.getCode())
				.build();
	}

	@ExceptionHandler(value = { JsonProcessingException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<Void> jsonProcessingException(JsonProcessingException e) {
		return ResponseEntity.badRequest().header("X-error-message", "Bad Json").header("X-error-code", "SYS_REQ_0")
				.build();
	}

	@ExceptionHandler(value = { MethodArgumentTypeMismatchException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<Void> jsonProcessingException(MethodArgumentTypeMismatchException e) {
		return ResponseEntity.badRequest().header("X-error-message", "Bad parameter")
				.header("X-error-code", "SYS_REQ_0").build();
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = { DecodingException.class })
	public ResponseEntity<Void> jsonDecodingException(DecodingException e) {
		return ResponseEntity.badRequest().header("X-error-message", "Bad Json").header("X-error-code", "SYS_REQ_0")
				.build();
	}

	@ExceptionHandler(value = { NoMoreCapacityException.class })
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	public ResponseEntity<Void> notRunTimeException(NoMoreCapacityException ex) {
		return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).header("X-error-message", ex.getMessage())
				.header("X-error-code", ex.getCode()).build();
	}


	@ExceptionHandler(value = { NotAcceptedException.class })
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	public ResponseEntity<Void> notAcceptedException(NotAcceptedException ex) {
		return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).header("X-error-message", ex.getMessage())
				.header("X-error-code", ex.getCode()).build();
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = { DataIntegrityViolationException.class })
	public ResponseEntity<Void> validationExceptionException(DataIntegrityViolationException e) {
		return ResponseEntity.badRequest().header("X-error-message", e.getMessage()).header("X-error-code", "SYS_REQ_3")
				.build();
	}

	@ExceptionHandler(value = { IllegalArgumentException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<Void> illegalArgumentException(IllegalArgumentException ex) {
		return ResponseEntity.badRequest().header("X-error-message", ex.getMessage())
				.header("X-error-code", "SYS_REQ_2").build();
	}

	@ExceptionHandler(value = { HttpMessageNotReadableException.class })
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	public ResponseEntity<Void> notRunTimeException(HttpMessageNotReadableException ex) {
		return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).header("X-error-message", ex.getMessage())
				.header("X-error-code", "SYS_REQ_1").build();
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = { ValidationException.class })
	public ResponseEntity<Void> validationExceptionException(ValidationException e) {
		return ResponseEntity.badRequest().header("X-error-message", e.getMessage()).header("X-error-code", "SYS_REQ_1")
				.build();
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ConstraintViolationException.class)
	protected ResponseEntity<Void> handleConstraintViolation(ConstraintViolationException ex) {

		String errors = ex.getConstraintViolations().stream().map(ConstraintViolation::getMessage)
				.collect(Collectors.joining());
		return ResponseEntity.badRequest().header("X-error-message", errors).header("X-error-code", "SYS_REQ_1")
				.build();
	}


	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = { IllegalStateException.class })
	public ResponseEntity<Void> illegalStatusException(IllegalStateException ex) {

		log.info("handling Exception code: {} message : {}", "SYS_EXE_3", ex.getMessage());
		log.error("handling Exception code", "SYS_EXE_0", ex);

		return ResponseEntity.badRequest().header("X-error-message", ex.getMessage())
				.header("X-error-code", "SYS_EXE_3").build();
	}


	@ExceptionHandler(value = { RuntimeException.class })
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<Void> notRunTimeException(RuntimeException ex) {
		log.info("handling Exception code: {} message : {}", "SYS_EXE_1", ex.getMessage());
		log.error("handling Exception code", "SYS_EXE_1", ex);

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("X-error-message", ex.getMessage())
				.header("X-error-code", "SYS_EXE_1").build();
	}

	@ExceptionHandler(value = { Exception.class })
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<Void> notRunTimeException(Exception ex) {
		log.info("handling Exception code: {} message : {}", "SYS_EXE_0", ex.getMessage());
		log.error("handling Exception code", "SYS_EXE_0", ex);

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("X-error-message", ex.getMessage())
				.header("X-error-code", "SYS_EXE_0").build();
	}

//	public Error getError(AbstractArmException ex) {
//		log.debug("handling Arm Exception ... {}", ex);
//		return new Error(ex.getCode(),
//				messageSource.getMessage(ex.getMessageCode(), ex.getArgs(), Locale.getDefault()));
//	}
//
//	public Error getError(String code, String message, Exception ex) {
//
//		log.info("handling Exception code: {} message : {}", code, ex.getMessage());
//		log.error("handling Exception code", code, ex);
//
//		return new Error(code, messageSource.getMessage(message, null, Locale.getDefault()));
//	}
//
//	public Error getError(String code, String message, Exception ex, Object... args) {
//
//		log.info("handling Exception code: {} message : {}", code, ex.getMessage());
//		log.error("handling Exception code", code, ex);
//		return new Error(code, messageSource.getMessage(message, args, Locale.getDefault()));
//	}



}
