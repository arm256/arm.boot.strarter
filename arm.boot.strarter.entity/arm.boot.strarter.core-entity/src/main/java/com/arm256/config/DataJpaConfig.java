package com.arm256.config;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.arm256.entity.ArmObjectEntity;
import com.arm256.entity.EventEntity;
import com.arm256.model.ArmObject;
import com.arm256.model.mapper.metadata.MetadataValueMapper;

@EnableSpringDataWebSupport
@Configuration
@EntityScan(basePackageClasses = { EventEntity.class })
@EnableJpaAuditing
public class DataJpaConfig {

	@Bean
	public AuditorAware<String> auditor() {
			return new AuditorAware<String>() {
				
				@Override
				public Optional<String> getCurrentAuditor() {
					try {
						ServletRequestAttributes curentRequest = (ServletRequestAttributes) RequestContextHolder
								.currentRequestAttributes();
						return Optional.ofNullable(curentRequest)
							.map(ServletRequestAttributes::getRequest).map(e -> e.getHeader("userId"));
				} catch (IllegalStateException e) {
						return Optional.of("SYSTEM");
					}
					
				}
			};
		
	}

	@Autowired
	protected ModelMapper modelMapper;
	@Autowired
	private MetadataValueMapper metadataValueMapper;

	@PostConstruct
	public void init() {
		modelMapper.addMappings(new PropertyMap<ArmObjectEntity, ArmObject>() {

			@Override
			protected void configure() {
				using(metadataValueMapper).map(source.getMetadata()).setMetadata(null);
			}
		});
	}

}
