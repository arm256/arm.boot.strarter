package com.arm256.repository;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.LockModeType;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.NoRepositoryBean;

import com.arm256.entity.Loadable;

@NoRepositoryBean
@Transactional
public interface Generic<T extends Loadable<ID>, ID extends Serializable>
		extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public Optional<T> findByIdAndArchiveFalse(ID id);
}
