package com.arm256.repository;

import java.util.UUID;

import org.springframework.data.repository.NoRepositoryBean;

import com.arm256.entity.Loadable;

@NoRepositoryBean
public interface GenericUUID<T extends Loadable<UUID>> extends Generic<T, UUID> {

}
