package com.arm256.repository;

import org.springframework.data.repository.NoRepositoryBean;

import com.arm256.entity.Loadable;

@NoRepositoryBean
public interface GenericLong<T extends Loadable<Long>> extends Generic<T, Long> {

}
