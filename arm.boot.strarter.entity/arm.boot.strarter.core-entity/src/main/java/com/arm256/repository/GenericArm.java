package com.arm256.repository;

import org.springframework.data.repository.NoRepositoryBean;

import com.arm256.entity.ArmObjectEntity;

@NoRepositoryBean
public interface GenericArm<T extends ArmObjectEntity> extends GenericUUID<T> {

}
