package com.arm256.entity.metadata;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.arm256.entity.ArmObjectEntity;
import com.arm256.entity.Loadable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "metadatavalue")
public class MetadataValueEntity implements Loadable<Long> {

	@Id
    @Column(name = "metadata_value_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "metadatavalue_seq")
    @SequenceGenerator(name = "metadatavalue_seq", sequenceName = "metadatavalue_seq", allocationSize = 1)
	private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "metadata_field_id")
    private MetadataFieldEntity metadataField = null;

    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    @Column(name = "text_value")
    private String value;

    @Column(name = "text_lang", length = 24)
    private String language;

    @Column(name = "place")
	@Builder.Default
	private int place = 0;

	@Builder.Default
	private int placeGroup = 0;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "arm_object_id")
	protected ArmObjectEntity armObject;

	private boolean archive;

}
