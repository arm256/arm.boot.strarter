package com.arm256.entity.metadata;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.arm256.entity.Loadable;
import com.arm256.utils.Pair;
import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "fieldvalidator")

@TypeDefs({ @TypeDef(name = "string-array", typeClass = StringArrayType.class),
		@TypeDef(name = "int-array", typeClass = IntArrayType.class) })
public class MetadataFieldValidatorsEntity implements Loadable<Long> {

	@Id
	@Column(name = "field_validator_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "field_validator_seq")
	@SequenceGenerator(name = "field_validator_seq", sequenceName = "field_validator_seq", allocationSize = 1, initialValue = 1)
	private Long id;

	@Column(name = "name", length = 64, unique = true)
	private String name;


	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fieldValidator", cascade = CascadeType.ALL, orphanRemoval = true)
	@Builder.Default
	private List<ParamsValueEntity> params = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "field_id")
	private MetadataFieldEntity fieldObject;

	public Pair<String, String>[] readParams() {
		return params.stream().map(e -> Pair.of(e.getParam(), e.getValue())).toArray(Pair[]::new);
	}
}
