package com.arm256.entity.metadata;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.arm256.entity.Loadable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "metadataschemaregistry")
public class MetadataSchemaEntity implements Loadable<Long> {

    @Id
    @Column(name = "metadata_schema_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "metadataschemaregistry_seq")
    @SequenceGenerator(name = "metadataschemaregistry_seq", sequenceName = "metadataschemaregistry_seq",
        allocationSize = 1)
	private Long id;

    @Column(name = "namespace", unique = true, length = 256)
    private String namespace;

    @Column(name = "short_id", unique = true, length = 32)
    private String name;

	private boolean archive;
}
