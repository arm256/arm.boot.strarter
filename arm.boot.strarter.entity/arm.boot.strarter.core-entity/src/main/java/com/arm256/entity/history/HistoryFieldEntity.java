package com.arm256.entity.history;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.arm256.entity.Loadable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HistoryFieldEntity implements Loadable<Long> {

    @Id
	@Column(name = "history_field_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "historyfieldregistry_seq")
	@SequenceGenerator(name = "historyfieldregistry_seq", sequenceName = "historyfieldregistry_seq", allocationSize
        = 1, initialValue = 1)
	private Long id;

	@Column(name = "name", length = 64, unique = true)
	private String name;

	private int maxRetry;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fieldObject", cascade = CascadeType.ALL, orphanRemoval = true)
	@Builder.Default
	private List<HistoryRowsEntity> historyRows = new ArrayList<>();

	private boolean archive;

	public void addHistoryRows(HistoryRowsEntity add) {
		historyRows.add(add);
	}

}
