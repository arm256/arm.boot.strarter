package com.arm256.entity.metadata;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.arm256.entity.Loadable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "paramvalue")
public class ParamsValueEntity implements Loadable<Long> {

	@Id
    @Column(name = "metadata_value_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "paramvalue_seq")
	@SequenceGenerator(name = "paramvalue_seq", sequenceName = "paramvalue_seq", allocationSize = 1)
	private Long id;

	@Column(name = "key")
	private String param;

	@Column(name = "value")
    private String value;


	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "field_validator_id")
	protected MetadataFieldValidatorsEntity fieldValidator;


}
