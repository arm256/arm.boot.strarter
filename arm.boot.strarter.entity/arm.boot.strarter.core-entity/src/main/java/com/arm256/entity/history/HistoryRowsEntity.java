package com.arm256.entity.history;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.arm256.entity.Loadable;
import com.arm256.enumation.ExcuteStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "historyrow")
public class HistoryRowsEntity implements Loadable<Long> {

	@Id
	@Column(name = "history_rows_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "history_rows_seq")
	@SequenceGenerator(name = "history_rows_seq", sequenceName = "history_rows_seq", allocationSize = 1, initialValue = 1)
	private Long id;

	@Column(unique = true)
	private UUID trackingId;

	private LocalDateTime createdDate;

	private String createdBy;

	private ExcuteStatus status = ExcuteStatus.PROGRESS;

	private int retryTimes;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "field_id")
	private HistoryFieldEntity fieldObject;

	private boolean archive;

}
