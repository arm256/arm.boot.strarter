package com.arm256.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.arm256.entity.metadata.MetadataValueEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@EntityListeners(AuditingEntityListener.class)
@Table(name = "armobject")
public abstract class ArmObjectEntity extends AbstractAuditableEntity {

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "armObject", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("metadataField, place")
	@Builder.Default
	private List<MetadataValueEntity> metadata = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "auditableObject", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("createdDate")
	@Builder.Default
	private List<EventEntity> events = new ArrayList<>();

	@Transient
	@Setter(AccessLevel.NONE)
	private boolean modified = false;

	@Transient
	@Setter(AccessLevel.NONE)
	private StringBuffer eventDetails = null;

	@Transient
	private String scopeNote = null;

	public String eventData() {
		return null;
	};

	@PreUpdate
	public void preUpdate() {
		if (modified) {
			String details = getDetails();
			if (details != null || (details = eventData()) != null) {
				events.add(EventEntity.builder().auditableObject(this).data(details).scopeNote(scopeNote).build());
				clearDetails();
				clearModified();
				clearScopeNotes();
			}
		}
	}




	public void setMetadata(List<MetadataValueEntity> metadata) {
		this.metadata = metadata;
	}

	public void removeMetadata(MetadataValueEntity metadataValue) {
		madeModified();
		getMetadata().remove(metadataValue);
	}

	public void removeMetadata(List<MetadataValueEntity> metadataValues) {
		madeModified();
		getMetadata().removeAll(metadataValues);
	}

	public void addMetadata(MetadataValueEntity metadataValue) {
		madeModified();
		getMetadata().add(metadataValue);
	}

	public void addDetails(String d) {
		if (eventDetails == null) {
			eventDetails = new StringBuffer(d);
		} else {
			eventDetails.append(", ").append(d);
		}
	}

	public String getDetails() {
		return (eventDetails == null ? null : eventDetails.toString());
	}

	public void clearDetails() {
		eventDetails = null;
	}

	public void clearModified() {
		this.modified = false;
	}

	private void clearScopeNotes() {
		this.scopeNote = null;
	}

	protected void madeModified() {
		this.modified = true;
	}

}
