package com.arm256.entity;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.springframework.data.annotation.CreatedDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@Entity(name = "event_history")
public class EventEntity implements Loadable<Long> {

	@Id
	@Column(name = "event_history_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_history_seq")
	@SequenceGenerator(name = "event_history_seq", sequenceName = "event_history_seq", allocationSize = 1, initialValue = 1)
	private Long id;

	@CreatedDate
	private LocalDateTime createdDate;

	@Lob
	@Column(name = "dataBefore")
	private String data;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "events_id")
	protected ArmObjectEntity auditableObject;

	@Column(name = "scope_note", columnDefinition = "text")
	private String scopeNote;

}
