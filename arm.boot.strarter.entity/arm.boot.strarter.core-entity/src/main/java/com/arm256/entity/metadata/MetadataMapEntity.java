package com.arm256.entity.metadata;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.arm256.entity.Loadable;
import com.arm256.model.MetadataMap.FieldsType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "metadatamapregistry")
public class MetadataMapEntity implements Loadable<Long> {

    @Id
	@Column(name = "metadata_map_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "metadatamapregistry_seq")
	@SequenceGenerator(name = "metadatamapregistry_seq", sequenceName = "metadatamapregistry_seq", allocationSize
        = 1, initialValue = 1)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "metadata_schema_id", nullable = false)
    private MetadataSchemaEntity metadataSchema;


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "metadata_field_id")
	private MetadataFieldEntity metadataField = null;

	private FieldsType fieldType;
	@ElementCollection(targetClass = String.class)
	private List<String> defaultValues;

	private String mapField;

	private String pointer;

	private String type;

	@Builder.Default
	private boolean required = false;

	private boolean archive;

}
