package com.arm256.entity.metadata;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.http.HttpMethod;

import com.arm256.entity.Loadable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "request_info_registry")
public class RequestInfoEntity implements Loadable<Long> {

	@Id
	@Column(name = "request_info_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "requestinforegistry_seq")
	@SequenceGenerator(name = "requestinforegistry_seq", sequenceName = "requestinforegistry_seq", allocationSize = 1, initialValue = 1)
	private Long id;

	@Column(unique = true)
	private String identity;

	private String uri;
	private HttpMethod httpMethod;
	private int httpStatus;

	private String errorId;
	private String succeedId;

	@ElementCollection(targetClass = String.class)
	private List<String> bodyId = new ArrayList<>();

	private String startPointer;

	private boolean archive;


}
