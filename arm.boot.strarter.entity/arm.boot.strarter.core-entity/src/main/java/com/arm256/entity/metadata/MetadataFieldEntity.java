package com.arm256.entity.metadata;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.arm256.entity.Loadable;
import com.arm256.model.MetadataField.FieldsTypeView;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "metadatafieldregistry", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "metadata_schema_id", "element", "qualifier" }) })
public class MetadataFieldEntity implements Loadable<Long> {

    @Id
    @Column(name = "metadata_field_id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "metadatafieldregistry_seq")
    @SequenceGenerator(name = "metadatafieldregistry_seq", sequenceName = "metadatafieldregistry_seq", allocationSize
        = 1, initialValue = 1)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "metadata_schema_id", nullable = false)
    private MetadataSchemaEntity metadataSchema;

    @Column(name = "element", length = 64)
    private String element;

    @Column(name = "qualifier", length = 64)
    private String qualifier = null;

	private Class<?> clazzType;

	private FieldsTypeView fieldTypeView;

    @Column(name = "scope_note", columnDefinition = "text")
    private String scopeNote;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fieldObject", cascade = CascadeType.ALL, orphanRemoval = true)
	@Builder.Default
	private List<MetadataFieldValidatorsEntity> validators = new ArrayList<>();

	private boolean archive;

}
