package com.arm256.model.mapper;

import com.arm256.entity.ArmObjectEntity;
import com.arm256.model.ArmObject;

public abstract class AbstractArmMapper<E extends ArmObjectEntity, D extends ArmObject> extends AbstractMapper<E, D> {

//	@Autowired
//	private MetadataValueMapper metadataValueMapper;

	public AbstractArmMapper(Class<E> eClazz, Class<D> dClazz) {
		super(eClazz, dClazz);
	}

//	@PostConstruct
//	public void init() {
//
//		modelMapper.addMappings(new PropertyMap<ArmObjectEntity, ArmObject>() {
//
//			@Override
//			protected void configure() {
//				using(metadataValueMapper).map(source.getMetadata()).setMetadata(null);
//			}
//		});
//	}


}
