package com.arm256.model.mapper.metadata;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.arm256.entity.metadata.MetadataValueEntity;
import com.arm256.model.MetadataValue;
import com.arm256.model.mapper.AbstractMapperList;

@Component
@Qualifier("metadata_value-mapper")
public class MetadataValueMapper extends AbstractMapperList<MetadataValueEntity, MetadataValue> {

	public MetadataValueMapper() {
		super(MetadataValueEntity.class, MetadataValue.class);
	}

	@Override
	public MetadataValueEntity entityUpdate(MetadataValueEntity entity, MetadataValue dto) {
		entity.setLanguage(dto.getLanguage());
		entity.setPlace(dto.getPlace());
		entity.setValue(dto.getValue());
		return entity;
	}


}
