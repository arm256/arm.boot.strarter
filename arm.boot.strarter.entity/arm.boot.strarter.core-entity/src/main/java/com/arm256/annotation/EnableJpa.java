
package com.arm256.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.arm256.config.DataJpaConfig;
import com.arm256.config.ModelMapperConfig;
import com.arm256.config.RestProperties;
import com.arm256.model.mapper.metadata.MetadataValueMapper;


@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({ DataJpaConfig.class, ModelMapperConfig.class, RestProperties.class, MetadataValueMapper.class })
@EnableStoreServices
@EnableHistory
public @interface EnableJpa {

}
