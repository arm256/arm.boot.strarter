package com.arm256.controller;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.noContent;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.arm256.model.ModelLoadable;
import com.arm256.service.CRUD;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RequestMapping(consumes = "application/json", produces = "application/json")
public abstract class AbstractCRUDController<T extends ModelLoadable<E>, E extends Serializable> {

	protected final CRUD<?, T, E> service;

	@Autowired
	public AbstractCRUDController(CRUD<?, T, E> service) {
		this.service = service;
	}

	@GetMapping()
	public ResponseEntity<List<T>> all(@RequestParam(name = "page", defaultValue = "0", required = false) int pageNo,
			@RequestParam(name = "size", defaultValue = "20", required = false) int pageSize,
			HttpServletResponse response

//			@RequestParam(required = false) Pageable page
	) {

		Pageable page = PageRequest.of(pageNo, pageSize);
		Page<T> read = service.retrieves(page);

		if (read.isEmpty()) {
			return noContent().build();
		}
		HttpHeaders responseHeader = new HttpHeaders();

		responseHeader.add("X-content-total", "" + read.getTotalElements());
		responseHeader.add("X-page-total", "" + read.getTotalPages());
		responseHeader.add("X-content-size", "" + read.getNumberOfElements());

		return new ResponseEntity<List<T>>(read.getContent(), responseHeader, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	@Operation(parameters = {
			@Parameter(name = "id", in = ParameterIn.PATH, required = true, description = "uuid of element need to select", schema = @Schema(type = "string", format = "uuid", pattern = "/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"), example = "dd58a026-5d7e-4c42-b2f0-83081938903f") }, responses = @ApiResponse(responseCode = "200", description = "ok", headers = {}))
	public T get(@PathVariable("id") E queriesId) {
		return this.service.retrieve(() -> queriesId);
	}

	@PostMapping
	@Operation(responses = @ApiResponse(responseCode = "201", description = "Created", headers = {
			@Header(name = "location", required = true, description = "url of new data created")

	}))
	public ResponseEntity<Void> save(@RequestHeader(required = false) String userId, @RequestBody T form,
			HttpServletRequest request) {
		T dto = service.create(form);
		return created(ServletUriComponentsBuilder.fromContextPath(request).path(controllerPath() + "/{id}")
				.buildAndExpand(dto.getId()).toUri()).build();
	}

	@PutMapping("/{id}")
	@Operation(parameters = {
			@Parameter(name = "id", in = ParameterIn.PATH, required = true, description = "uuid of element need to select", schema = @Schema(type = "string", format = "uuid", pattern = "/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"), example = "dd58a026-5d7e-4c42-b2f0-83081938903f") }, responses = @ApiResponse(responseCode = "204", description = "updated"))
	public ResponseEntity<Void> update(@PathVariable("id") E id, @RequestBody T form) {
		form.setId(id);
		service.update(form);
		return noContent().build();
	}

	@DeleteMapping("/{id}")
	@Operation(responses = @ApiResponse(responseCode = "204", description = "deleted"))
	public ResponseEntity<Void> delete(@PathVariable("id") E id) {
		service.delete(() -> id);
		return noContent().build();
	}

	public <S extends CRUD<?, T, E>> S getService() {
		return (S) service;
	}

	public abstract String controllerPath();
}
