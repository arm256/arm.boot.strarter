package com.arm256.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.arm256.model.ModelLoadable;
import com.arm256.service.CRUD;

public abstract class AbstractCRUD_UUIDController<T extends ModelLoadable<UUID>>
		extends AbstractCRUDController<T, UUID> {

	@Autowired
	public AbstractCRUD_UUIDController(CRUD<?, T, UUID> service) {
		super(service);
	}

	public abstract String controllerPath();
}
