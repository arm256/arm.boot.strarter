package com.arm256.service;

import java.util.UUID;

import com.arm256.entity.Loadable;
import com.arm256.model.ModelLoadable;

public interface CRUD_UUID<E extends Loadable<UUID>, D extends ModelLoadable<UUID>> extends CRUD<E, D, UUID> {

}
