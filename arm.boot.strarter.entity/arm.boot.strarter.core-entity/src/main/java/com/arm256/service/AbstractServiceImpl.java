package com.arm256.service;

import java.io.Serializable;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.arm256.entity.Loadable;
import com.arm256.exception.NotFoundException;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.Mapper;
import com.arm256.repository.Generic;

public abstract class AbstractServiceImpl<E extends Loadable<ID>, D extends ModelLoadable<ID>, R extends Generic<E, ID>, ID extends Serializable>
		implements CRUD<E, D, ID> {

	protected R repository;
	protected Mapper<E, D> mapper;

	protected Supplier<NotFoundException> notFound = () -> new NotFoundException(name());
	protected Function<E, D> toDto = (e) -> mapper.toDto(e);
	protected Function<D, E> toEntity = (d) -> mapper.toEntity(d);
	public AbstractServiceImpl(R repository, Mapper<E, D> mapper) {
		this.repository = repository;
		this.mapper = mapper;
	}

	@Override
	public D create(D d) {
		return mapper.toDto(create(mapper.toEntity(d)));
	}

	@Override
	public E create(E e) {
		return repository.save(e);
	}

	@Override
	public Page<D> retrieves(Pageable pageable) {
		if (pageable == null) {
			pageable = PageRequest.of(0, 20);
		}
		return repository.findAll(pageable).map(mapper::toDto);
	}

	@Override
	public Page<D> retrieves(Specification<E> spec, Pageable pageable) {
		return repository.findAll(spec, pageable).map(mapper::toDto);
	}


	@Override
	public D retrieve(Loadable<ID> id) {
		return mapper.toDto(read(id));
	}

	@Override
	public void update(D d) {
		E entity = read(d);
		repository.save(mapper.entityUpdate(entity, d));
	}

	@Override
	public void delete(Loadable<ID> id) {
		repository.delete(read(id));
	}

	@Override
	public E read(Loadable<ID> id) {
		return repository.findByIdAndArchiveFalse(id.getId()).orElseThrow(notFound);
	}

	@SuppressWarnings("unchecked")
	@Override
	public R getRepository() {
		return repository;
	}

	@Override
	public Mapper<E, D> getMapper() {
		return mapper;
	}

	public abstract String name();

	protected List<E> notEmpty(List<E> entities) {
		if (entities.isEmpty()) {
			throw notFound.get();
		}
		return entities;
	}
}
