package com.arm256.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import com.arm256.entity.Loadable;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.Mapper;
import com.arm256.repository.Generic;

public interface CRUD<E extends Loadable<ID>, D extends ModelLoadable<ID>, ID extends Serializable> {

	default void createAll(Iterable<D> d) {
		d.forEach(this::create);
	}

	default void createAllEntity(Iterable<E> d) {
		d.forEach(this::create);
	}

	D create(D d);

	@Transactional
	E create(E e);

	Page<D> retrieves(Pageable pageable);

	Page<D> retrieves(Specification<E> spec, Pageable pageable);

	D retrieve(Loadable<ID> id);

	@Transactional
	void update(D d);

	@Transactional
	void delete(Loadable<ID> id);

	E read(Loadable<ID> id);

	public <R extends Generic<E, ID>> R getRepository();

	public Mapper<E, D> getMapper();

	default D toDto(E e) {
		return getMapper().toDto(e);
	}

	default E toEntity(D d) {
		return getMapper().toEntity(d);
	}

	default List<D> toDto(List<E> e) {
		return getMapper().toDto(e);
	}

	default List<E> toEntity(List<D> d) {
		return getMapper().toEntity(d);
	}

}
