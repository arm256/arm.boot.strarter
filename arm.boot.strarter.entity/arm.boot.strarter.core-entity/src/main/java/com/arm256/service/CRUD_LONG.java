package com.arm256.service;

import com.arm256.entity.Loadable;
import com.arm256.model.ModelLoadable;

public interface CRUD_LONG<E extends Loadable<Long>, D extends ModelLoadable<Long>> extends CRUD<E, D, Long> {

}
