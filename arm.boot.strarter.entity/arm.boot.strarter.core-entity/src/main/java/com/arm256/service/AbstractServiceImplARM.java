package com.arm256.service;

import com.arm256.entity.ArmObjectEntity;
import com.arm256.model.ArmObject;
import com.arm256.model.mapper.Mapper;
import com.arm256.repository.GenericArm;

public abstract class AbstractServiceImplARM<E extends ArmObjectEntity, D extends ArmObject, R extends GenericArm<E>>
		extends AbstractServiceImplUUID<E, D, R> implements CRUD_ARM<E, D> {

	public AbstractServiceImplARM(R repository, Mapper<E, D> mapper) {
		super(repository, mapper);
	}

}
