package com.arm256.service;

import com.arm256.entity.ArmObjectEntity;
import com.arm256.model.ArmObject;

public interface CRUD_ARM<E extends ArmObjectEntity, D extends ArmObject> extends CRUD_UUID<E, D> {

}
