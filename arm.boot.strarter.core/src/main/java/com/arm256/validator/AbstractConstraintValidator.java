package com.arm256.validator;

import java.util.ArrayList;
import java.util.List;

import com.arm256.utils.Pair;
import com.arm256.utils.JsonUtils;

public abstract class AbstractConstraintValidator<T> implements ConstraintValidator<T> {

	private List<Object[]> runtimeParams = new ArrayList<>();

	private ParamsConstraintValidator<?> paramValidator;

	private Class<T> clazzType;

	public AbstractConstraintValidator(ParamsConstraintValidator<?> paramValidator) {
		this.paramValidator = paramValidator;
		this.clazzType = JsonUtils.readGeneracClass(getClass());
		if (clazzType == null) {
			clazzType = (Class<T>) Object.class;
		}
	}

	public AbstractConstraintValidator() {
		paramValidator = null;
	}

	@SuppressWarnings("unchecked")
	public synchronized int initialize(Pair<String, String>... constraintParameter) {
		Object[] runtimeParam = paramValidator == null ? ParamsConstraintValidator.EMPTY
				: paramValidator.validatorParam(constraintParameter);
		if (runtimeParam == null) {
			return -1;
		}
		int index = runtimeParams.size();
		runtimeParams.add(runtimeParam);
		return index;
	}


	protected Object[] getParams(int index) {
		return runtimeParams.get(index);
	}

	@SuppressWarnings("unchecked")
	protected <E> E getParams(int index, int arrayIndex) {
		Object[] e = runtimeParams.get(index);
		if (e.length <= arrayIndex) {
			return null;
		}
		return (E) e[arrayIndex];
	}

	public Class<T> getClazzType() {
		if (clazzType == null) {
			return (Class<T>) Object.class;
		}
		return clazzType;
	}

	public ParamsConstraintValidator<?> getParamValidator() {
		return paramValidator;
	}
}
