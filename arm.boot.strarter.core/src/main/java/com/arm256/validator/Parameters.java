package com.arm256.validator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

public class Parameters<T extends Serializable> implements ParameterInterface<T> {


	@SuppressWarnings("unchecked")
	public Parameters(String name, boolean multi, boolean required, Class<T> clazzType) {
		this.name = name;
		this.clazzType = clazzType;
		this.multi = multi;
		this.required = required;
		this.index = readClassPosition(clazzType);
	}

	public Parameters(String name, boolean multi, boolean required, Class<T> clazzType, T dv) {
		this(name, multi, required, clazzType);
		defalutValue = dv;
	}

	private String name;

	private Class<T> clazzType;

	private boolean multi = false;

	private boolean required = false;

	private T defalutValue;

	@Getter(value = AccessLevel.NONE)
	@Setter(value = AccessLevel.NONE)
	private int index;

	public String getName() {
		return name;
	}

	public Class<T> getClazzType() {
		return clazzType;
	}

	public boolean isMulti() {
		return multi;
	}

	public boolean isRequired() {
		return required;
	}

	public T getDefalutValue() {
		return defalutValue;
	}

	@SuppressWarnings("unchecked")
	public List<T> converterValue(String value) {
		if (value == null || value.isEmpty()) {
			return Collections.emptyList();
		}
		if (multi) {
			List<T> arr = new ArrayList<>();
			for (String su : StringUtils.split(value, ',')) {
				arr.add((T) functions[index].apply(su));
			}
		}
		return Collections.singletonList((T) functions[index].apply(value));
	}

	public T converterValueFirst(String value) {
		List<T> list = converterValue(value);
		if (list.isEmpty()) {
			return null;
		}
		return converterValue(value).get(0);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <E extends Enum> Class<E> readEnumClazzType() {
		return (Class<E>) clazzType;
	}

	public int readClassPosition(Class<?> clazz) {
		int position = -1;

		for (Class<?> c : clazzs) {
				position++;
				if (clazz == c) {
					return position;
				}
			}

		return -1;
	}

	private Function<String, String> stringFun = e -> e;

	private Function<String, Integer> integerFun = e -> Integer.parseInt(e);

	private Function<String, Double> doubleFun = e -> Double.parseDouble(e);

	private Function<String, Long> longFun = e -> Long.parseLong(e);

	private Function<String, Boolean> booleanFun = e -> Boolean.parseBoolean(e);

	private Function<String, Enum<?>> enumFun = e -> Enum.valueOf(readEnumClazzType(), e);

	@SuppressWarnings("rawtypes")
	private Function[] functions = { stringFun, integerFun, doubleFun, longFun, booleanFun, enumFun };

	private Function<String, String> stringFunToString = e -> e;

	private Function<Integer, String> integerFunToString = e -> Integer.toString(e);

	private Function<Double, String> doubleFunToString = e -> Double.toString(e);

	private Function<Long, String> longFunToString = e -> Long.toString(e);

	private Function<Boolean, String> booleanFunToString = e -> Boolean.toString(e);

	private Function<Enum<?>, String> enumFunToString = e -> e.name();

	@SuppressWarnings("rawtypes")
	private Function[] functionsToString = { stringFun, integerFun, doubleFun, longFun, booleanFun, enumFun };

}
