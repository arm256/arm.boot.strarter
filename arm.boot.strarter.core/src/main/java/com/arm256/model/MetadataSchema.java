package com.arm256.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
@Getter
@Setter
public class MetadataSchema extends AbstractModel<Long> {

	private static final long serialVersionUID = -5249472256649044590L;

	@NonNull
	private String name;

	@NonNull
	private String namespace;


}
