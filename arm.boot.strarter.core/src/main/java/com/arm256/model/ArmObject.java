package com.arm256.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.arm256.utils.MapperUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuppressWarnings("serial")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
public abstract class ArmObject extends AbstractAuditableModel {
	@Builder.Default
	@JsonIgnore
	private List<MetadataValue> metadata = new ArrayList<>();

	public void addMetadata(MetadataValue metadataValue) {
		metadata.add(metadataValue);
	}

	public void addMetadata(List<MetadataValue> metadataValue) {
		metadata.addAll(metadataValue);
	}

	public MetadataValue readMetadata(String path) {
		return readMetadata(path, metadata);
	}

	public static MetadataValue readMetadata(String path, List<MetadataValue> metad) {
		for (MetadataValue mv : metad) {
			if (mv.sameField(path)) {
				return mv;
			}
		}
		return MetadataValue.Empty;
	}

	public List<MetadataValue> readMetadata() {
		return metadata;
	}

	public JsonNode getData() {
		return MapperUtil.valuesToJson(metadata);
	}

	public void setData(List<Map<String, String>> list) {
//		return MapperUtil.valuesToJson(metadata);
	}
}
