package com.arm256.model;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.arm256.exception.NotFoundException;
import com.fasterxml.jackson.core.JsonPointer;

import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
public class RequestInfo extends AbstractModel<Long> {

	private String identity;
	private String uri;
	private HttpMethod httpMethod;
	private HttpStatus httpStatus;
	private String errorId;
	private String succeedId;
	private List<String> bodyId = new ArrayList<>();

	private JsonPointer startPointer;

	public JsonPointer readStartPointer() {
		return startPointer;
	}

	public String getStartPointer() {
		if (startPointer == null) {
			return null;
		}
		return startPointer.toString();
	}

	public void setStartPointer(String startPointer) {
		if (startPointer != null) {
			this.startPointer = JsonPointer.compile(startPointer);
		}
	}

	public UriBuilder readUriBuilder() {
		return UriComponentsBuilder.fromHttpUrl(uri);
	}

	public HttpMethod readHttpMethod() {
		return httpMethod;
	}

	public HttpStatus readHttpStatus() {
		return httpStatus;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getHttpMethod() {
		return httpMethod.name();
	}

	public void setHttpMethod(String method) {
		this.httpMethod = HttpMethod.valueOf(method);
	}

	public int getHttpStatus() {
		return httpStatus.value();
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = HttpStatus.resolve(httpStatus);
	}

	public String getErrorId() {
		return errorId;
	}

	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}

	public String getSucceedId() {
		return succeedId;
	}

	public void setSucceedId(String succeedId) {
		this.succeedId = succeedId;
	}

	public List<String> getBodyId() {
		return bodyId;
	}

	public void setBodyId(List<String> bodyId) {
		this.bodyId = bodyId;
	}

	public void addBodyId(String bodyId) {

		if (this.bodyId == null) {
			synchronized (this) {
				this.bodyId = new ArrayList<String>();
			}
		}
		if (!this.bodyId.contains(bodyId))
			this.bodyId.add(bodyId);
	}

	public boolean containBodyId(String body) {

		if (this.bodyId == null) {
			return false;
		}
		return this.bodyId.contains(body);
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	private final Function<String, ? extends String> notFound = e -> {
		throw new NotFoundException(e);
	};

//	public RequestInfo(Map<String, String> rest) {
//		uri = rest.computeIfAbsent("url", notFound);
//		uriBuilder = UriComponentsBuilder.fromHttpUrl(uri);
//		htpMethod = HttpMethod.valueOf(rest.computeIfAbsent("method", notFound));
//		httpStatus = HttpStatus.resolve(Integer.parseInt(rest.computeIfAbsent("status", notFound)));
//
//		errorId = rest.computeIfAbsent("mapper-error", notFound);
//		succeedId = rest.get("mapper-succeed");
//		bodyId = rest.get("mapper-body");
//
//	}

}
