package com.arm256.model.mapper;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.arm256.entity.Loadable;
import com.arm256.model.ModelLoadable;

public interface Mapper<E extends Loadable<?>, D extends ModelLoadable<?>> {

	E entityUpdate(E entity, D dto);

	D toDto(E entity);

	E toEntity(D dto);

	default List<D> toDto(Collection<E> entities) {
		if (entities == null || entities.isEmpty()) {
			return Collections.emptyList();
		}
		return entities.stream().map(this::toDto).collect(Collectors.toList());
	}

	default List<E> toEntity(Collection<D> dto) {
		if (dto == null || dto.isEmpty()) {
			return Collections.emptyList();
		}
		return dto.stream().map(this::toEntity).collect(Collectors.toList());
	}

	default List<D> toDto(Iterable<E> entities) {
		if (entities == null) {
			return Collections.emptyList();
		}
		return StreamSupport.stream(entities.spliterator(), false).map(this::toDto).collect(Collectors.toList());
	}

	default List<E> toEntity(Iterable<D> dto) {
		if (dto == null) {
			return Collections.emptyList();
		}
		return StreamSupport.stream(dto.spliterator(), false).map(this::toEntity).collect(Collectors.toList());
	}
}
