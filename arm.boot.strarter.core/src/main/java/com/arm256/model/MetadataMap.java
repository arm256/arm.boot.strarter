package com.arm256.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonPointer;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(Include.NON_NULL)
public class MetadataMap extends AbstractModel<Long> {
	public final static MetadataMap Empty = new MetadataMap();

	private String schema;
	
//	@JsonProperty(access = Access.READ_ONLY)
	private MetadataField metadataField;

	private String mapField;

	@Getter(value = AccessLevel.NONE)
	@Setter(value = AccessLevel.NONE)
	private JsonPointer pointer;
	
	private String type;

	@Singular("defaultValue")
	private List<String> defaultValues;

	private boolean required;
	@Builder.Default
	private FieldsType fieldType = FieldsType.NUN;

	public static enum FieldsType {
		NUN(false), OBJECT(false), ARRAY(true), ARRAY_OBJECT(true), ARRAY_ARRAY(true);

		FieldsType(boolean array) {
			this.array = array;
		}

		private final boolean array;

		public boolean isArray() {
			return array;
		}
	}


	public FieldsType getFieldType() {
		if (fieldType == null) {
			fieldType = FieldsType.NUN;
		}
		return fieldType;
	}
	public void setPointer(String pointer) {
		if (pointer == null) {
			return;
		}
		this.pointer = JsonPointer.compile(pointer);
	}

	public String getPointer() {
		if (pointer == null) {
			return null;
		}
		return pointer.toString();
	}

	public JsonPointer readPointer() {
		return pointer;
	}

	public static class MetadataMapBuilder {
		public MetadataMapBuilder pointer(String point) {
			this.pointer = JsonPointer.compile(point);
			return this;
		}
	}

	public Class<?> readClassType() {
		return metadataField.getClazzType();
	}
//	@JsonProperty(access = Access.WRITE_ONLY)
//	public void setMetadataFieldPath(String path) {
//		metadataField = new MetadataField(path);
//	}
}
