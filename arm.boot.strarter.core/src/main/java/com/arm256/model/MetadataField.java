package com.arm256.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Builder
public class MetadataField extends AbstractModel<Long> {

	public static final MetadataField Empty = new MetadataField();
	
	@EqualsAndHashCode.Exclude
	@Getter(value = AccessLevel.NONE)
	@Setter(value = AccessLevel.NONE)
	private String path;
	
//	private int index;

    private String schema;

    private String element;

    private String qualifier = null;

	@EqualsAndHashCode.Exclude
    private String scopeNote;
    
	@EqualsAndHashCode.Exclude
	@Builder.Default
	private Class<?> clazzType = String.class;
	
	@JsonProperty(access = Access.READ_ONLY)
	@EqualsAndHashCode.Exclude
	@Singular("validator")
	private List<FieldValidators> validators;
	
	@Builder.Default
	private FieldsTypeView fieldTypeView = FieldsTypeView.NORMAL;
	
	public MetadataField(String path) {
		this.path = path;
	}
	
	@JsonProperty(access = Access.READ_ONLY)
	public String getElementPath() {
		if (path == null) {
			path = schema + "." + element + (qualifier == null ? "" : "." + qualifier);
		}
		return path;
	}

	public boolean equal(String path) {
		return getElementPath().equals(path);

	}

	public boolean equal(String schema, String element, String qualifier) {
		return this.schema.equals(schema) && equal(element, qualifier);

	}

	public boolean equal(String element, String qualifier) {
		return this.element.equals(element)
				&& (this.qualifier == null ? this.qualifier == qualifier : this.qualifier.equals(qualifier));

	}
	
	public enum FieldsTypeView{
		NORMAL,SCHEMA_ARRAY,ELEMENTS_ARRAY,QUALIFIRE_ARRAY ,HIDE
	}

}
