package com.arm256.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.util.List;
import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@Builder
@ToString
public class MetadataValue extends AbstractModel<Long> {

	public static final MetadataValue Empty = new MetadataValue() {
		{
			setMetadataField(MetadataField.Empty);
		}
	};

	@JsonProperty(access = Access.WRITE_ONLY)
	private MetadataField metadataField;

	private String value;

	private String language;

	@JsonProperty(access = Access.WRITE_ONLY)
	private int place = 0;

	@JsonProperty(access = Access.WRITE_ONLY)
	@Builder.Default
	private int placeGroup = 0;

	@JsonProperty(access = Access.READ_ONLY)
	public String getElement() {
		return metadataField.getElementPath();
	}

	public String toString() {
		return getElement() + " = " + value + " (place:" + place + ",group:" + placeGroup + ")";
	}

	public MetadataValue(String value) {
		this.value = value;
	}

	public boolean sameField(String path) {
		return metadataField.equal(path);
	}
	
	public static MetadataValueKeeper valueInstance(String path, String value) {
		return new MetadataValueKeeper(path, value);
	}
	
	public static class MetadataValueKeeper extends MetadataValue {

		public MetadataValueKeeper(String path, String value) {
			elementPath = path;
			super.value = value;
		}

		private String elementPath;

		@Override
		public String getElement() {
			return elementPath;
		}

		@Override
		public boolean sameField(String path) {
			return elementPath.equals(path);
		}
	}
	
	@Getter
	@Setter
	@AllArgsConstructor
	public static class MetadataValueLite {
		private String path;
		private String value;	
		
		MetadataValue toMetadataValue(){
			return valueInstance(path, value);
		}
		
	}
	
	
	public MetadataValueLite toMetadataValueLite() {
		return new MetadataValueLite(getElement(), value);
	}
	
	public static List<MetadataValue> loadData(String[][] data){
		
		List<MetadataValue> values = new ArrayList<>(data.length);
		for (int i = 0; i < data.length; i++) {
			values.add(valueInstance(data[i][0],data[i][1]));
		}
		return values;
	}

}
