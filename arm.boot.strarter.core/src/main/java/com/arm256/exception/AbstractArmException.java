package com.arm256.exception;

import lombok.Getter;

@SuppressWarnings("serial")
@Getter
public abstract class AbstractArmException extends RuntimeException {
	private final String code;
	private final String messageCode;

	private Object[] args;

	public AbstractArmException(String code, String messageCode) {
		this.code = code;
		this.messageCode = messageCode;
	}

	public AbstractArmException(String code, String messageCode, String message) {
		super(message);
		this.code = code;
		this.messageCode = messageCode;
	}

	public AbstractArmException setArgs(Object... args) {
		this.args = args;
		return this;
	}

}
