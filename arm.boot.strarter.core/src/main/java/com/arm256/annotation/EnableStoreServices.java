
package com.arm256.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.arm256.controller.StoreController;
import com.arm256.service.StoreMetadataFieldService;
import com.arm256.service.StoreMetadataMapService;
import com.arm256.service.StoreRequestInfoService;


@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({ StoreMetadataMapService.class, StoreRequestInfoService.class, StoreMetadataFieldService.class,
		StoreController.class })
public @interface EnableStoreServices {

}
