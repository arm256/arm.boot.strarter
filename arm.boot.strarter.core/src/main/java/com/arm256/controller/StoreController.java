package com.arm256.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arm256.annotation.SwaggerController;
import com.arm256.model.MetadataField;
import com.arm256.model.MetadataMap;
import com.arm256.model.RequestInfo;
import com.arm256.service.StoreMetadataFieldService;
import com.arm256.service.StoreMetadataMapService;
import com.arm256.service.StoreRequestInfoService;

@RestController
@RequestMapping(StoreController.path)
@SwaggerController
public class StoreController {
	public final static String path = "v1/store";

	@Autowired
	private StoreMetadataFieldService fieldStoreService;

	@Autowired
	private StoreMetadataMapService mapStoreService;

	@Autowired
	private StoreRequestInfoService requestInfoStoreService;

	@GetMapping("/fields")
	public Map<String, List<MetadataField>> readStoreFields() {
		return fieldStoreService.readAll();
	}

	@GetMapping("/maps")
	public Map<String, List<MetadataMap>> readStoreMaps() {
		return mapStoreService.readAll();
	}

	@GetMapping("/requests")
	public Map<String, List<RequestInfo>> readStoreRequestInfo() {
		return requestInfoStoreService.readAll();
	}
}
