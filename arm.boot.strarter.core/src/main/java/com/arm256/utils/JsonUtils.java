package com.arm256.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.function.Function;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lombok.Getter;
import lombok.experimental.UtilityClass;
import sun.reflect.generics.reflectiveObjects.GenericArrayTypeImpl;

@UtilityClass
public class JsonUtils {

	private final static JsonNodeFactory nodeFactory = JsonNodeFactory.instance;


	public static Function<String, JsonNode> stringFun = e -> {
		return nodeFactory.textNode(e);
	};

	public static Function<String, JsonNode> integerFun = e -> {
		return nodeFactory.numberNode(Integer.parseInt(e));
	};

	public static Function<String, JsonNode> doubleFun = e -> {
		return nodeFactory.numberNode(Double.parseDouble(e));
	};
	public static Function<String, JsonNode> longFun = e -> {
		return nodeFactory.numberNode(Long.parseLong(e));
	};
	public static Function<String, JsonNode> booleanFun = e -> {
		return nodeFactory.booleanNode(Boolean.parseBoolean(e));
	};

	@SuppressWarnings("unchecked")
	private static final Function<String, JsonNode>[] readNodeValue = new Function[] { stringFun, integerFun, doubleFun,
			longFun,
			booleanFun
	};


	@SuppressWarnings("unchecked")
	public static JsonNode readNodeValue(String value, Class<?> clazz) {

		int index = SupportClazz.readClassIndex(clazz);

		return readNodeValue[index].apply(value);
	}

	public static JsonNode at(JsonPointer ptr, JsonNode node) {

		if (ptr.matches()) {
			return node;
		}

		JsonNode n = null;

		if (node.isObject()) {
			n = node.get(ptr.getMatchingProperty());
			if (n == null) {
				n = ((ObjectNode) node).putObject(ptr.getMatchingProperty());
			}
		} else if (node.isArray()) {
			n = node.get(ptr.getMatchingIndex());
		}

		if (n == null) {
			return MissingNode.getInstance();
		}

		return at(ptr.tail(), n);
	}

	@Getter
	private final static ObjectMapper mapper = new ObjectMapper();

	public <T> Class<T> readGeneracClass(Class<?> clazz) {
		Type genericSuperClass = clazz.getGenericSuperclass();
		if (genericSuperClass.equals(Object.class)) {
			return (Class<T>) Object.class;
		}
		ParameterizedType parametrizedType = null;
		while (parametrizedType == null) {
			if ((genericSuperClass instanceof ParameterizedType)) {
				parametrizedType = (ParameterizedType) genericSuperClass;
			} else {
				genericSuperClass = ((Class<?>) genericSuperClass).getGenericSuperclass();
			}
		}
		Type param = parametrizedType.getActualTypeArguments()[0];
		if (param instanceof GenericArrayTypeImpl) {
			GenericArrayTypeImpl p = (GenericArrayTypeImpl) param;
			return (Class<T>) p.getClass();
		}

		return (Class<T>) parametrizedType.getActualTypeArguments()[0];

	}

	public final static ObjectNode createJson() {
		return mapper.createObjectNode();
	}

	public <D> D readObject(JsonNode json, Class<D> clazz) {
		try {
			return mapper.treeToValue(json, clazz);
		} catch (JsonProcessingException e) {
			return null;
		}
	}

	public final static JsonNode createJson(String content) {
		try {
			return mapper.readTree(content);
		} catch (JsonProcessingException e) {
			return NullNode.getInstance();
		}
	}

}
