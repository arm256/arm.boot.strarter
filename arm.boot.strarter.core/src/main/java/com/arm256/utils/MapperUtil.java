package com.arm256.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiConsumer;

import com.arm256.exception.NotFoundException;
import com.arm256.exception.UnexpectedTypeException;
import com.arm256.model.MetadataField;
import com.arm256.model.MetadataField.FieldsTypeView;
import com.arm256.model.MetadataMap;
import com.arm256.model.MetadataValue;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lombok.experimental.UtilityClass;

/**
 * @author ARM256
 *
 */
/**
 * @author ARM256
 *
 */
@UtilityClass
public class MapperUtil {

	public static enum FieldLevel {
		FULL_PATH {
			@Override
			boolean equal(MetadataField field, String path) {
				return path.equals(field.getSchema());
			}

			@Override
			FieldLevel nextLevel() {
				return null;
			}
		},
		SCHEMA {
			@Override
			boolean equal(MetadataField field, String path) {
				return path.equals(field.getSchema());
			}

			@Override
			FieldLevel nextLevel() {
				return ELEMENT;
			}
		},
		ELEMENT {
			@Override
			boolean equal(MetadataField field, String path) {
				return path.equals(field.getElement());
			}

			@Override
			FieldLevel nextLevel() {
				return QUALIFIER;
			}
		},
		QUALIFIER {
			@Override
			boolean equal(MetadataField field, String path) {
				return path.equals(field.getQualifier());
			}

			@Override
			FieldLevel nextLevel() {
				return NON;
			}
		},
		NON {
			@Override
			boolean equal(MetadataField field, String path) {
				return false;
			}

			@Override
			FieldLevel nextLevel() {
				return null;
			}
		};

		abstract boolean equal(MetadataField field, String path);

		abstract FieldLevel nextLevel();

	}

	public static List<MetadataValue> readDataValuesByFields(List<MetadataField> fields, JsonNode jsonEntity,
			List<MetadataValue> metadataValues, FieldLevel level, BiConsumer<List<String>, MetadataField> validator) {

		Iterator<Entry<String, JsonNode>> json = jsonEntity.fields();

		Map.Entry<String, JsonNode> entry;
		String k; // json key { "key" : {"A" : 2 } }
		JsonNode v; // json value { "key" : "value" }
		List<String> values = null;
		while (json.hasNext()) {
			entry = json.next();
			k = entry.getKey();
			v = entry.getValue();
			if (v.isObject()) {
				if (v.isEmpty()) {
					continue;
				}
				if (level.nextLevel() != null) {
					readDataValuesByFields(fields, v, metadataValues, level.nextLevel(), validator);
					continue;
				}
			}

			fieldLoop: for (MetadataField mf : fields) {

				if (!level.equal(mf, k)) {
					continue;
				}

//				values = MapperUtil.readNodeValue(v, mf.getClazzType());
//				validator.accept(values, mf);
//
//				int place = 0;
//				for (String mv : values) {
//					metadataValues.add(MetadataValue.builder().metadataField(mf).value(mv).place(place).build());
//					place++;
//				}

				addValueFromNode(metadataValues, v, mf, null, false, validator, 0);
				break fieldLoop;
			}

		}
		return metadataValues;
	}

	/**
	 * @param metadataValues values that you want to add new value to
	 * @param nodeV          - json node value
	 * @param mf             - field of converter
	 * @param mapPointer     - any json map to this value (Optional)
	 * @param required       - this value required or Not (if value not Exits and
	 *                       it's Required it will throw NotFound Exceptions)
	 * @param validator      Consumer of (List of field validators)
	 * @return true if value added false if Missing
	 */
	public static boolean addValueFromNode(List<MetadataValue> metadataValues, JsonNode nodeV, MetadataField mf,
			JsonPointer mapPointer, boolean required, BiConsumer<List<String>, MetadataField> validator,
			int group) {

		Class<?> fieldClass = mf.getClazzType();

		nodeV = mapPointer == null ? nodeV : nodeV.at(mapPointer);

		if (nodeV.isMissingNode()) {
			if (required) {
				throw new NotFoundException(mf.getElementPath() + " Json node");
			}
			return false;
		}

		List<String> values = MapperUtil.readNodeValue(nodeV, fieldClass);

		validator.accept(values, mf);
		int place = 0;

		for (String mv : values) {
			metadataValues
					.add(MetadataValue.builder().metadataField(mf).value(mv).place(place).placeGroup(group).build());
			place++;
		}

		return true;
	}

	public static JsonNode valuesToJson(List<MetadataValue> values) {

		ObjectNode json = JsonUtils.createJson();

		for (MetadataValue mv : values) {
			addValueToNode(mv, json, mv.getMetadataField(), true);
		}
		return json;

	}

	public static JsonNode valuesToJsonIgnoreSchema(List<MetadataValue> values) {
		System.out.println(values);
		ObjectNode json = JsonUtils.createJson();

		for (MetadataValue mv : values) {
			addValueToNode(mv, json, mv.getMetadataField(), false);
		}
		return json;

	}

	public static void addValueToNode(MetadataValue mv, ObjectNode node, MetadataField field, boolean addSchema) {

		if (field.getFieldTypeView() == FieldsTypeView.HIDE) {
			return;
		}
		JsonNode valueNode = JsonUtils.readNodeValue(mv.getValue(), field.getClazzType());

		ObjectNode schemaN;

		if (addSchema) {
			schemaN = (ObjectNode) node.get(field.getSchema());

			if (schemaN == null) {
				schemaN = node.putObject(field.getSchema());
			}

		} else {
			schemaN = node;
		}

		JsonNode elementN = schemaN.get(field.getElement());

		element:
		if (elementN == null) {

			if (field.getFieldTypeView() == FieldsTypeView.ELEMENTS_ARRAY) {
				elementN = schemaN.putArray(field.getElement());
				break element;
			}

			if (field.getQualifier() == null) {
				schemaN.put(field.getElement(), valueNode);
				return;
			}
			elementN = schemaN.putObject(field.getElement());

		}

		if (!(elementN.isObject() || elementN.isArray())) {
			String tmplVE = elementN.asText();
			((ObjectNode) schemaN).putArray(field.getElement())
					.add(JsonUtils.readNodeValue(tmplVE, field.getClazzType())).add(valueNode);
			return;
		}

		if (elementN.isArray()) {

			if (field.getQualifier() == null) {
				((ArrayNode) elementN).add(valueNode);
				return;
			}

			int s = elementN.size() - mv.getPlaceGroup();
			ObjectNode tmp = null;
			if (s <= 0) {
				for (; s < 1; s++) {
					tmp = ((ArrayNode) elementN).addObject();
				}
			}

			if (tmp == null) {
				tmp = (ObjectNode) elementN.get(mv.getPlaceGroup());
			}

			elementN = tmp;
		}

		JsonNode qualifierN = elementN.get(field.getQualifier());

		if (qualifierN == null) {

			((ObjectNode) elementN).put(field.getQualifier(), valueNode);
			return;
		}

		if (qualifierN.isArray()) {
			((ArrayNode) qualifierN).add(valueNode);
			return;
		}

		String tmplValue = qualifierN.asText();
		if (tmplValue == null || tmplValue.isEmpty()) {
			return;
		}

		((ObjectNode) elementN).putArray(field.getQualifier())
				.add(JsonUtils.readNodeValue(tmplValue, field.getClazzType())).add(valueNode);

	}

	public static void addValueToNode(MetadataValue value, ObjectNode json, MetadataMap mm) {

		String name = mm.getMapField();
		JsonPointer point = mm.readPointer();

		JsonNode node = point == null ? json.path(name) : JsonUtils.at(point, json);

		// No values added before
		if (node.isMissingNode()) {
			json.put(name, value.getValue());
			return;
		}
		JsonNode NodeValue = JsonUtils.readNodeValue(value.getValue(), mm.readClassType());
		// Arrays of values
		int index = value.getPlace();
		switch (mm.getFieldType()) {

		case ARRAY: {
			JsonNode tmpNode = node.get(name);
			if (tmpNode == null) {
				ArrayNode arrayNode = ((ObjectNode) node).putArray(name);
				arrayNode.add(NodeValue);
			} else {
				((ArrayNode) tmpNode).add(NodeValue);
			}
		}
			break;
		case ARRAY_ARRAY:
			if (!node.isArray()) {
				if (point == null) {
					node = ((ObjectNode) json).putArray(name);
				} else {
					node = ((ObjectNode) json.at(point.head())).putArray(name);
				}
			}
			ArrayNode jnode = (ArrayNode) ((ArrayNode) node).get(index);
			while (jnode == null) {
				((ArrayNode) node).addArray();
				jnode = (ArrayNode) ((ArrayNode) node).get(index);
			}
			jnode.add(NodeValue);

			break;
		case ARRAY_OBJECT: {
			ObjectNode onode = (ObjectNode) ((ArrayNode) node).get(index);
			while (onode == null) {
				((ArrayNode) node).addObject();
				onode = (ObjectNode) ((ArrayNode) node).get(index);
			}
			onode.put(name, NodeValue);
		}

			break;

		default: {
			JsonNode tmpNode = node.get(name);
			if (tmpNode == null) {
				if (node.isArray())
					((ArrayNode) node).add(NodeValue);
				else
					((ObjectNode) node).put(name, NodeValue);
			} else if (tmpNode.isArray()) {
				((ArrayNode) tmpNode).add(NodeValue);
			} else {
				String tmplValue = tmpNode.asText();
				if (point == null) {
					((ObjectNode) json).putArray(name).add(JsonUtils.readNodeValue(tmplValue, mm.readClassType()))
							.add(NodeValue);
				} else {
					((ObjectNode) json.at(point.head())).putArray(name)
							.add(JsonUtils.readNodeValue(tmplValue, mm.readClassType())).add(NodeValue);
				}
			}
		}
			break;
		}

	}

	/**
	 * @param json NodeJson that you want to read value from .
	 * @param type Type of value class .
	 * @return list of value or singleton list if classType single .
	 * @throws UnexpectedTypeException if data type not corrected or not exits .
	 */
	@SuppressWarnings("unchecked")
	public static List<String> readNodeValue(JsonNode json, Class<?> type) {

		String fun;

		for (int i = 0; i < SupportClazz.clazzs.length; i++) {

			if (!SupportClazz.clazzs[i].isAssignableFrom(type)) {
				continue;
			}

			fun = SupportClazz.functions[i].apply(json);

			if (fun == null) {
				throw new UnexpectedTypeException("Data Type Not Corrected , please check this json:  " + json
						+ " Matches with this type: " + type);
			}

			if (!List.class.isAssignableFrom(type)) {
				// single type
				return Collections.singletonList(fun);

			}

			// list type
			List<String> list = new ArrayList<>();

			listLoop: for (int j = 0; j < SupportClazz.clazzsList.length; j++) {

				if (!SupportClazz.clazzsList[j].equals(type)) {
					continue;
				}

				for (JsonNode jsonNode : json) {

					fun = SupportClazz.functions[j].apply(jsonNode);

					if (fun == null) {
						throw new UnexpectedTypeException("Data Type Not Corrected , please check this json:  " + json
								+ " Matches with this type: " + type);
					}

					list.add(fun);
				}

				break listLoop;
			}

			return list;

		}

		throw new UnexpectedTypeException("No data type found for json:  " + json);
	}
}
