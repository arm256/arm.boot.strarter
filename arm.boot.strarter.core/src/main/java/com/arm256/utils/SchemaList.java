package com.arm256.utils;

import java.util.ArrayList;
import java.util.List;

import com.arm256.model.MetadataSchema;

public class SchemaList extends ArrayList<MetadataSchema> {

	private static final long serialVersionUID = -3258712047257614840L;

	private final List<MetadataSchema> schemas = new ArrayList<>();

	public void addSchema(MetadataSchema schema) {
		int index = schemas.size();


		schemas.add(schema);
	}
}
