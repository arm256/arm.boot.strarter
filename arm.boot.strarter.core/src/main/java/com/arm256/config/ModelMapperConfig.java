package com.arm256.config;

import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.arm256.model.MetadataMap;

@Configuration
public class ModelMapperConfig {

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	public Map<String, List<MetadataMap>> metaDataMaps() {
		return new IdentityHashMap<String, List<MetadataMap>>();
	}

}
