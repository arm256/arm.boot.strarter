package com.arm256.config;

import java.security.InvalidParameterException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import lombok.Data;

@Configuration
@ConfigurationProperties(prefix = "arm.rest")
@Data
public class RestProperties {
	
	private String sslProtocol = "TLS";
	private int readTimout = 5000;
	private int connectTimeout = 3000;

	@Bean
	@Scope("prototype")
	public RestTemplate restTemplete() {

		SSLContext context = null;
		try {
			context = SSLContext.getInstance(sslProtocol);
			context.init(null, null, null);
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			throw new InvalidParameterException();
		}
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setReadTimeout(readTimout);
		factory.setConnectTimeout(connectTimeout);

		CloseableHttpClient httpClient = HttpClientBuilder.create().setMaxConnTotal(100).setMaxConnPerRoute(5)
				.setSSLContext(context).build();

		factory.setHttpClient(httpClient);

		return new RestTemplate(factory);
	}

}
