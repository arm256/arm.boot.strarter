package com.arm256.config;

import java.time.temporal.Temporal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

import com.arm256.annotation.SwaggerController;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

	@Bean
	public Docket api() {

		return new Docket(DocumentationType.SWAGGER_2).forCodeGeneration(true)
//				.securitySchemes(Arrays.asList(new ApiKey("Token Access", HttpHeaders.AUTHORIZATION, In.HEADER.name())))
				.useDefaultResponseMessages(false).select()
				.apis(RequestHandlerSelectors.withClassAnnotation(SwaggerController.class)).build()
				.genericModelSubstitutes(ResponseEntity.class).directModelSubstitute(Temporal.class, String.class);

//    ParameterBuilder paramBuilder = new ParameterBuilder();
//    List<Parameter> params = new ArrayList<>();
//		Parameter au = paramBuilder.name("Authorization").modelRef(new ModelRef("string")).parameterType("header")
//				.required(false).build();
//		params.add(au);
//		au = paramBuilder.name("userId").modelRef(new ModelRef("string")).parameterType("header").required(true)
//				.build();
//		params.add(au);
//		
//		
//		 docket.forCodeGeneration(true|false);
//
//    return new Docket(DocumentationType.SWAGGER_2).globalOperationParameters(params)
//				.select()
//				.apis(RequestHandlerSelectors.withClassAnnotation(SwaggerController.class))
//        .paths(PathSelectors.any())
//				.build();

	}

}
