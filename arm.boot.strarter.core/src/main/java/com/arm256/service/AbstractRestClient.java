package com.arm256.service;

import java.net.URI;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriBuilder;

import com.arm256.exception.NotFoundException;
import com.arm256.model.ArmObject;
import com.arm256.model.MetadataMap;
import com.arm256.model.MetadataValue;
import com.arm256.model.RequestInfo;
import com.arm256.utils.JsonUtils;
import com.arm256.utils.MapperUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Log4j2
public abstract class AbstractRestClient {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private StoreMetadataMapService metadataService;

	@Autowired
	private StoreRequestInfoService requestInfoaService;

	private final MappingData mappingService;

	public AbstractRestClient(MappingData mappingService) {
		this.mappingService = mappingService;
	}

	protected static final Function<String, ? extends String> notFound = e -> {
		throw new NotFoundException(e);
	};

	public abstract void preExecute(RequestInfo restInfo, List<MetadataValue> request);

	public abstract <D> D onError(RequestPrepare request, ResponseEntity<JsonNode> response, Class<D> clazzType);

	public Function<JsonNode, Boolean> hasError() {
		return e -> false;
	}

	public List<List<MetadataValue>> executeAndMapList(String request, List<MetadataValue> values) {

		RequestInfo restInfo = requestInfoaService.readFirst(request);
		JsonNode node = execute(restInfo, values, JsonNode.class);
		return mappingService.readDataValuesListByMap(node, restInfo.getSucceedId(), restInfo.readStartPointer());
	}

	public List<MetadataValue> executeAndMap(String request, List<MetadataValue> values) {

		RequestInfo restInfo = requestInfoaService.readFirst(request);
		JsonNode node = execute(restInfo, values, JsonNode.class);
		return mappingService.readDataValuesByMap(node, restInfo.getSucceedId(), restInfo.readStartPointer());
	}

	public <D extends ArmObject> D executeAndMap(String request, List<MetadataValue> values, D d) {

		RequestInfo restInfo = requestInfoaService.readFirst(request);
		JsonNode node = execute(restInfo, values, JsonNode.class);
		if (restInfo.getSucceedId() == null) {
			return d;
		}
		return mappingService.mapingDataValues(node, restInfo.getSucceedId(), restInfo.readStartPointer(), d);
	}

	public JsonNode execute(String request, List<MetadataValue> values) {
		return execute(request, values, JsonNode.class);
	}

	public <D> D executeAndMapFun(String request, List<MetadataValue> values, Function<JsonNode, D> mappedValue) {
		return mappedValue.apply(execute(request, values, JsonNode.class));
	}

	public <D> D execute(String request, List<MetadataValue> values, Class<D> clazzType) {
		RequestInfo restInfo = requestInfoaService.readFirst(request);
		return execute(restInfo, values, clazzType);
	}

	public <D> D execute(RequestInfo restInfo, @NonNull List<MetadataValue> values, Class<D> clazzType) {

		final RequestPrepare request = new RequestPrepare(restInfo);
		preExecute(restInfo, values);
		log.info("Request info and values RequestInfo {} \n values : {} ", restInfo, values);

		List<MetadataMap> bodyRequest = restInfo.getBodyId() == null ? Collections.emptyList()
				: metadataService.read(restInfo.getBodyId());

		BiConsumer<MetadataMap, MetadataValue> valueMapping = (mm, v) -> request.isolateValues(mm, v);

		mappingService.mappingData(bodyRequest, values, valueMapping);


		ResponseEntity<JsonNode> response = executeRequest(request, JsonNode.class);
		log.info("Response : {}  ", response);

		if (response.getStatusCode() != restInfo.readHttpStatus()) {
			return onError(request, response, clazzType);
		}

		JsonNode responseBody = response.getBody();

		if (clazzType.isAssignableFrom(JsonNode.class)) {

			if (hasError().apply(responseBody)) {
				return onError(request, response, clazzType);
			}
			return (D) responseBody;
		}

		return JsonUtils.readObject(responseBody, clazzType);

	}

	public <D> ResponseEntity<D> executeRequest(RequestPrepare request, Class<D> clazzType) {
		return restTemplate.exchange(request.readURI(), request.getRestInfo().readHttpMethod(), request.readEntity(),
				clazzType);
	}

	public class RequestPrepare {
		Map<String, String> uriMap;
		MultiValueMap<String, String> queryParam;
		MultiValueMap<String, String> header;
		ObjectNode body;
		@Getter
		private final RequestInfo restInfo;

		public RequestPrepare(RequestInfo restInfo) {
			this.restInfo = restInfo;
		}

		public URI readURI() {

			log.info("Request prepare values URIMap : {} \n QueryParam : {} ", uriMap, queryParam);

			UriBuilder uriB = restInfo.readUriBuilder();

			if (queryParam != null) {
				uriB = uriB.queryParams(queryParam);
			}

			URI uri = uriMap != null ? uriB.build(uriMap) : uriB.build();

			log.info("Request uri : {} ", uri);

			return uri;

		}

		public HttpEntity<JsonNode> readEntity() {
			log.info("Request prepare values Header : {} \n Body: {} ", header, body);
			return new HttpEntity<JsonNode>(body, header);
		}

		public void isolateValues(MetadataMap map, MetadataValue value) {
			String type = map.getType() == null ? "" : map.getType();

			switch (type) {
			case "URI":
				addURI(map.getMapField(), value.getValue());
				break;
			case "PARAM":
				addQueryParam(map.getMapField(), value.getValue());
				break;

			case "HEADER":
				addHeader(map.getMapField(), value.getValue());
				break;
			default:
				addBody(map, value);
			}
		}

		public void addURI(String map, String value) {
			if (uriMap == null) {
				synchronized (RequestPrepare.this) {
					uriMap = new LinkedHashMap<String, String>();
				}
			}
			uriMap.put(map, value);
		}

		public void addQueryParam(String map, String value) {
			if (queryParam == null) {
				synchronized (RequestPrepare.this) {
					queryParam = new LinkedMultiValueMap<String, String>();
				}
			}
			queryParam.add(map, value);
		}

		public void replaceOrAddQueryParam(String map, String value) {
			if (queryParam == null) {
				synchronized (RequestPrepare.this) {
					queryParam = new LinkedMultiValueMap<String, String>();
				}
			}
			queryParam.set(map, value);
		}

		public void addHeader(String map, String value) {
			if (header == null) {
				synchronized (RequestPrepare.this) {
					header = new LinkedMultiValueMap<String, String>();
				}
			}
			header.add(map, value);
		}

		public void replaceOrAddHeader(String map, String value) {
			if (header == null) {
				synchronized (RequestPrepare.this) {
					header = new LinkedMultiValueMap<String, String>();
				}
			}
			header.set(map, value);
		}

		public void addBody(MetadataMap map, MetadataValue value) {
			if (body == null) {
				synchronized (RequestPrepare.this) {
					body = JsonUtils.createJson();
				}
			}
			MapperUtil.addValueToNode(value, body, map);
		}

	}

}
