package com.arm256.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiConsumer;

import com.arm256.exception.NotFoundException;
import com.arm256.model.ArmObject;
import com.arm256.model.MetadataField;
import com.arm256.model.MetadataMap;
import com.arm256.model.MetadataMap.FieldsType;
import com.arm256.model.MetadataValue;
import com.arm256.utils.JsonUtils;
import com.arm256.utils.MapperUtil;
import com.arm256.utils.MapperUtil.FieldLevel;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public interface MappingData {

	public List<MetadataMap> readMetadataMap(String schema);

	public List<MetadataField> readMetadataField(String schema);

	public default BiConsumer<List<String>, MetadataField> validate() {
		return (e, m) -> {
		};
	}

	public default List<List<MetadataValue>> readDataValuesListByMap(JsonNode jsonEntity, String schema,
			JsonPointer startPoint) {
		JsonNode data = startPoint == null ? jsonEntity : jsonEntity.at(startPoint);
		List<MetadataMap> metaData = readMetadataMap(schema);

		if (!data.isArray()) {
			return Collections.emptyList();
		}

		List<List<MetadataValue>> values = new ArrayList<>();

		for (JsonNode jsonNode : data) {
			values.add(readDataValuesByMap(metaData, jsonNode));
		}

		return values;
	}

	public default List<MetadataValue> readDataValuesByMap(JsonNode jsonEntity, String schema, JsonPointer startPoint) {
		JsonNode data = startPoint == null ? jsonEntity : jsonEntity.at(startPoint);
		return readDataValuesByMap(readMetadataMap(schema), data);
	}

	public default List<MetadataValue> readDataValuesByFields(JsonNode jsonEntity, String schema,
			JsonPointer startPoint) {
		JsonNode data = startPoint == null ? jsonEntity : jsonEntity.at(startPoint);
		List<MetadataValue> metadataValues = new ArrayList<>();
		return MapperUtil.readDataValuesByFields(readMetadataField(schema), data, metadataValues, FieldLevel.FULL_PATH,
				validate());
	}

	public default List<MetadataValue> readDataValuesByFields(JsonNode jsonEntity, JsonPointer startPoint) {
		JsonNode data = startPoint == null ? jsonEntity : jsonEntity.at(startPoint);
		if (data == null) {
			return Collections.emptyList();
		}
		Iterator<Entry<String, JsonNode>> json = data.fields();
		List<MetadataValue> metadataValues = new ArrayList<>();
		String k;
		Entry<String, JsonNode> entry;
		while (json.hasNext()) {
			entry = json.next();
			k = entry.getKey();
			MapperUtil.readDataValuesByFields(readMetadataField(k), entry.getValue(), metadataValues,
					FieldLevel.ELEMENT, validate());
		}
		return metadataValues;
	}

	public default <D extends ArmObject> D mapingDataValues(JsonNode json, String schema, JsonPointer startPoint, D d) {
		d.addMetadata(readDataValuesByMap(json, schema, startPoint));
		return d;
	}

	public default JsonNode mappingData(List<MetadataMap> map, List<MetadataValue> values) {
		ObjectNode json = JsonUtils.createJson();
		mappingData(map, values, (mm, v) -> MapperUtil.addValueToNode(v, json, mm));
		return json;
	}

	public default void mappingData(List<MetadataMap> map, List<MetadataValue> values,
			BiConsumer<MetadataMap, MetadataValue> addvalue) {

		if (map.isEmpty()) {
			return;
		}

		int[] markers = new int[map.size()];
		for (MetadataValue metadataValue : values) {
			nested: for (int i = 0; i < map.size(); i++) {

				MetadataMap mm = map.get(i);
				if (markers[i] == -1 || !metadataValue.sameField(mm.getMetadataField().getElementPath())) {
					continue;
				}

				addvalue.accept(mm, metadataValue);
				if (mm.getFieldType() == FieldsType.NUN) {
					markers[i] = -1;
				} else {
					markers[i] = 1;
				}

				break nested;
			}
		}
		for (int i = 0; i < map.size(); i++) {
			if (markers[i] == 0) {
				MetadataMap mm = map.get(i);
				List<String> defaultValue = mm.getDefaultValues();
				if (defaultValue != null) {
					for (String v : defaultValue) {
						addvalue.accept(mm, MetadataValue.builder().value(v).build());
					}
				}
			}
		}

	}

	public default List<MetadataValue> readDataValuesByMap(List<MetadataMap> map, JsonNode jsonEntity) {
		Iterator<Entry<String, JsonNode>> json = jsonEntity.fields();
		Map.Entry<String, JsonNode> entry;
		String k; // json key { "key" : {"A" : 2 } }
		JsonNode v; // json value { "key" : "value" }

		String mapField;
		JsonPointer mapPointer;
		List<String> values = null;
		Class<?> fieldClass;
		FieldsType fieldType;
		MetadataField mf;
		List<MetadataValue> metadataValues = new ArrayList<>();
		int place = 0;
		while (json.hasNext()) {
			entry = json.next();
			k = entry.getKey();
			loop:
			for (MetadataMap mm : map) {

				mapField = mm.getMapField();
				if (!k.equals(mapField)) {
					continue;
				}
				mapPointer = mm.readPointer();
				mf = mm.getMetadataField();
				fieldClass = mf.getClazzType();
				fieldType = mm.getFieldType();
				if (fieldType == FieldsType.ARRAY_ARRAY || fieldType == FieldsType.ARRAY_OBJECT) {

					for (JsonNode jsonVal : entry.getValue()) {
						MapperUtil.addValueFromNode(metadataValues, jsonVal, mf, mapPointer, mm.isRequired(),
								validate(), place);
						place++;
					}
					place = 0;
					continue;
				}
				MapperUtil.addValueFromNode(metadataValues, entry.getValue(), mf, mapPointer, mm.isRequired(),
						validate(), 0);
			}
		}
		return metadataValues;
	}

	public default void addValue(List<MetadataValue> metadataValues, MetadataMap mm, JsonNode v) {

		JsonPointer mapPointer = mm.readPointer();
		Class<?> fieldClass = mm.getMetadataField().getClazzType();

		v = mapPointer == null ? v : v.at(mapPointer);

		if (v.isMissingNode()) {
			if (mm.isRequired()) {
				throw new NotFoundException(mm.getMapField() + " Json node");
			}
			return;
		}

		List<String> values = MapperUtil.readNodeValue(v, fieldClass);
		validate().accept(values, mm.getMetadataField());
		int place = 0;
		for (String mv : values) {
			metadataValues
					.add(MetadataValue.builder().metadataField(mm.getMetadataField()).value(mv).place(place).build());
			place++;
		}
	}
}
