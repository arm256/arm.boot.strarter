package com.arm256.service;

import java.util.List;
import java.util.function.BiConsumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.arm256.model.ArmObject;
import com.arm256.model.MetadataField;
import com.arm256.model.MetadataMap;
import com.arm256.validator.ValidatorsService;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;

@Component
@Primary
public abstract class AbstractMappingDataModel<D extends ArmObject> implements MappingData {

	@Autowired
	private ValidatorsService validatorService;

	@Autowired
	private StoreMetadataMapService mapService;

	@Autowired
	private StoreMetadataFieldService fieldService;

	public D readMetadataFromMap(JsonNode node, String startPointer, D d) {
		d.addMetadata(readDataValuesByFields(node, JsonPointer.compile(startPointer)));
		return d;
	}

	@Override
	public List<MetadataMap> readMetadataMap(String schema) {
		return mapService.read(schema);
	}

	@Override
	public List<MetadataField> readMetadataField(String schema) {
		return fieldService.read(schema);
	}

	@Override
	public BiConsumer<List<String>, MetadataField> validate() {
		return (values,field)->{
		if (List.class.isAssignableFrom(field.getClazzType())) {
			validatorService.validate(values, field);
		} else {
			validatorService.validate(values.get(0), field);
			}
		};
	}


}
