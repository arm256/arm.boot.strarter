package com.arm256.service;

import org.springframework.stereotype.Service;

import com.arm256.model.MetadataField;

@Service
public class StoreMetadataFieldService extends AbstractStoreMapService<MetadataField> {

}
