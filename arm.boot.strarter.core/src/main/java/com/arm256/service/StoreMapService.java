package com.arm256.service;

import java.util.List;

public interface StoreMapService<T> {

	public List<T> read(String schema);

	public void add(String schema, List<T> d);

	public void add(String schema, T d);
}
